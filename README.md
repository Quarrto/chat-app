# chat-app

A messenger fullstack web application 

Live demo: https://firstchatapplication.netlify.app/

### Built with

- MongoDB
- Node.js, Express.js
- REST API, WebSocket
- Angular 12
