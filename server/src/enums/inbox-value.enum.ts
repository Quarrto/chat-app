export enum InboxValueEnum {
    ALL,
    FRIENDS,
    UNREAD,
    IMPORTANT
}