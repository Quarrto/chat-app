import { Router } from 'express';
import { AccountModel } from '../models/account.model';
import { InboxValueEnum } from '../enums/inbox-value.enum';
import { AccountInterface } from '../intefaces/account.interface';
import { MessageModel } from '../models/message.model';
import { Schema } from 'mongoose';
import { ChatModel } from '../models/chat.model';
import { UserInfoModel } from '../models/user-info.model';
import { socketMap } from '../index';

export const chatRouter = Router();

// friendRouter.get('/getChatsByFields/:login', async (req, res) => {
//     try {
//         const login = req.params.login;
//         const token = req.headers.authorization.slice(7);
//         const fields = req.query.fields ? req.query.fields.toString().split(',').map(value => +value) :
//             [InboxValueEnum.ALL, InboxValueEnum.FRIENDS, InboxValueEnum.IMPORTANT, InboxValueEnum.UNREAD];
//
//         const account = await AccountModel.findOne({login, sessionToken: token});
//
//         if (!account) {
//             res.status(401).send('Unauthorized');
//             return;
//         }
//
//         const result = {};
//
//         for (const value of fields) {
//             const previe
//             result[value] = await getChatIdsByField(account, value);
//         }
//
//         res.status(200).send(result));
//     } catch (e) {
//         res.status(500).send(e);
//     }
// });

async function getChatIdsByField(account: AccountInterface & { _id }, value: InboxValueEnum): Promise<Schema.Types.ObjectId[]> {
    let chats: any;

    switch (value) {
        case InboxValueEnum.ALL:
            return account.chats;
        case InboxValueEnum.FRIENDS:
            // chats = await ChatModel.find({participants: {$in: account.friends}});
            chats = await ChatModel.find({$and: [{participants: {$in: account._id}}, {participants: {$in: account.friends}}]});
            return chats.map(value1 => value1._id);
        case InboxValueEnum.IMPORTANT:
            // chats = await ChatModel.find({participants: {$in: account.important}});
            chats = await ChatModel.find({$and: [{participants: {$in: account._id}}, {participants: {$in: account.important}}]});
            return chats.map(value1 => value1._id);
        case InboxValueEnum.UNREAD:
            const chatsIds = account.chats;
            const unreadMessages = await MessageModel.find({
                chatId: {$in: chatsIds},
                read: false,
                ownerId: {$ne: account._id}
            });
            const unreadMessagesIds = unreadMessages.map((value1) => value1._id);
            chats = await ChatModel.find({messages: {$in: unreadMessagesIds}});
            return chats.map(value1 => value1._id);
        default:
            return [];
    }
}


chatRouter.get('/getChatsPreviewByField/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);
        const field = +req.query.field || InboxValueEnum.ALL;

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const chatsIds = await getChatIdsByField(account, field);

        // const result = chatsIds.map(async (id) => {
        //    return await getChatPreview(account._id, id);
        // });

        const result = [];

        for (const id of chatsIds) {
            result.push(await getChatPreview(account._id, id));
        }

        res.status(200).send(result);
    } catch (e) {
        res.status(500).send(e);
    }

});

async function getChatPreview(account: AccountInterface & { _id }, chatId: Schema.Types.ObjectId) {
    const chat = await ChatModel.findById(chatId);
    const [lastMessage] = await MessageModel.find({chatId}).sort({'date': -1}).limit(1);
    const chatMemberId = chat.participants.find(value => value.toString() !== account._id.toString());

    const {login, userInfo: userId} = await AccountModel.findById(chatMemberId);
    const user = await UserInfoModel.findById(userId);

    const connectionLogins = Array.from(socketMap.values());
    const unreadCount = await MessageModel.count({chatId, ownerId: chatMemberId, read: false});

    return {
        chatId: chatId.toString(),
        login,
        name: user?.name,
        surname: user?.surname,
        isImportant: account.important?.includes(chatMemberId) || false,
        online: connectionLogins.includes(login),
        unread: unreadCount,
        message: !lastMessage ? undefined : {
            message: lastMessage?.message,
            owner: lastMessage?.ownerId.toString() === account._id.toString() ? account.login : login,
            date: lastMessage?.date,
            read: lastMessage?.read
        }
    }
}

chatRouter.get('/getChat/:login/:chatId', async (req, res) => {
    try {
        const login = req.params.login;
        const chatId = req.params.chatId.toString();
        const token = req.headers.authorization.slice(7);
        const limit = +req.query.limit || 0;
        const offset = +req.query.offset || 0;

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const chat = await ChatModel.findById(chatId);

        if (!chat) {
            res.status(404).send('Chat not found');
            return;
        }

        if (!chat.participants.find(value => value.toString() === account._id.toString())) {
            res.status(403).send('You do not have access to this chat');
            return;
        }

        const chatMemberId = chat.participants.find(value => value.toString() !== account._id.toString());
        const {login: chatMemberLogin, userInfo: userId} = await AccountModel.findById(chatMemberId);

        const messagesCount = await MessageModel.count({chatId: chat._id});
        const messages = await MessageModel.find({chatId: chat._id}).sort({date: -1}).skip(offset).limit(limit);
        const user = await UserInfoModel.findById(userId);

        const connectionLogins = Array.from(socketMap.values());

        const result = {
            chatId: chat._id.toString(),
            login: chatMemberLogin,
            name: user?.name,
            surname: user?.surname,
            isImportant: account.important.includes(chatMemberId),
            online: connectionLogins.includes(chatMemberLogin),
            count: messagesCount,
            messages: messages.map((value) => {
                return {
                    message: value.message,
                    owner: value.ownerId.toString() === account._id.toString() ? account.login : chatMemberLogin,
                    date: value.date,
                    read: value.read
                }
            }).reverse()
        };

        res.status(200).send(result);

    } catch (e) {
        res.status(500).send(e);
    }
});

chatRouter.get('/getMessages/:login/:chatId', async (req, res) => {
    try {
        const login = req.params.login;
        const chatId = req.params.chatId.toString();
        const token = req.headers.authorization.slice(7);
        const limit = +req.query.limit || 0;
        const offset = +req.query.offset || 0;

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const chat = await ChatModel.findById(chatId);

        if (!chat) {
            res.status(404).send('Chat not found');
            return;
        }

        if (!chat.participants.find(value => value.toString() === account._id.toString())) {
            res.status(403).send('You do not have access to this chat');
            return;
        }

        const chatMemberId = chat.participants.find(value => value.toString() !== account._id.toString());
        const {login: chatMemberLogin} = await AccountModel.findById(chatMemberId);

        const messagesCount = await MessageModel.count({chatId: chat._id});
        const messages = await MessageModel.find({chatId: chat._id}).sort({date: -1}).skip(offset).limit(limit);

        const result = {
            chatId: chat._id.toString(),
            count: messagesCount,
            messages: messages.map((value) => {
                return {
                    message: value.message,
                    owner: value.ownerId.toString() === account._id.toString() ? account.login : chatMemberLogin,
                    date: value.date,
                    read: value.read
                }
            }).reverse()
        };


        res.status(200).send(result);

    } catch (e) {
        res.status(500).send(e);
    }
});
