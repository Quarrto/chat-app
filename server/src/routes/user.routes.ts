import { Router } from 'express';
import { AccountModel } from '../models/account.model';
import { UserInfoModel } from '../models/user-info.model';

export const userRouter = Router();

userRouter.put('/setUserInfo/:login/', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const {name, surname, gender, dateOfBirth, phoneNumber, country, city, languages} = req.body;

        const userInfo = await UserInfoModel.findByIdAndUpdate(
            account.userInfo,
            {
                name,
                surname,
                gender,
                dateOfBirth,
                phoneNumber,
                country,
                city,
                languages
            },
            {
                projection: ['-_id', '-__v'],
                new: true
            }
        );

        res.status(200).send(userInfo);
    } catch (e) {
        res.status(500).send(e);
    }

});

userRouter.get('/getUserInfo/:login/', async (req, res) => {
    try {
        const login = req.params.login;
        // const fields = '-_id -__v';
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const infoAccount = await AccountModel.findOne({login});

        if (!infoAccount) {
            res.status(404).send('User not found');
            return;
        }

        const userInfo = await UserInfoModel.findOne(
            {_id: infoAccount.userInfo}
        ).exec();

        res.status(200).send({
            name: userInfo.name,
            surname: userInfo.surname,
            gender: userInfo.gender,
            dateOfBirth: userInfo.dateOfBirth,
            phoneNumber: userInfo.phoneNumber,
            country: userInfo.country,
            city: userInfo.city,
            languages: userInfo.languages,
            isImportant: account.important?.includes(infoAccount._id) || false,
            isFriend: account.friends?.includes(infoAccount._id) || false,
            isPendingFriend: account.pendingFriends?.includes(infoAccount._id) || false,
            isSentFriend: account.sentFriends?.includes(infoAccount._id) || false

        });
    } catch (e) {
        res.status(500).send(e);
    }
});
