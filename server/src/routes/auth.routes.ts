import bcrypt from 'bcrypt'
import crypto from 'crypto';
import { Router } from 'express'
import { GenderEnum } from '../enums/gender.enum';
import { AccountModel } from '../models/account.model';
import { UserInfoModel } from '../models/user-info.model';

export const authRouter = Router();

authRouter.post('/register/', async (req, res) => {
    try {
        const {login, password, name, surname, gender, dateOfBirth, phoneNumber, country, city, languages} = req.body;

        if (!(login?.trim() && password?.trim())) {
            res.status(400).send('Login and password are required');
            return
        }

        const existUser = await AccountModel.findOne({login});

        if (existUser) {
            res.status(409).send('User Already Exist');
            return
        }

        const newUserInfo = new UserInfoModel({
            name: name?.trim() || undefined,
            surname: surname?.trim() || undefined,
            gender: gender || GenderEnum.UNMARKED,
            dateOfBirth,
            phoneNumber,
            country: country?.trim() || undefined,
            city: city?.trim() || undefined,
            languages
        });

        await newUserInfo.save();

        const encryptedPassword = await bcrypt.hash(password, 10);
        const token = crypto.randomBytes(64).toString('hex');

        const newAccount = new AccountModel({
            login,
            password: encryptedPassword,
            sessionToken: token,
            userInfo: newUserInfo._id
        });

        await newAccount.save();

        res.status(200).send({
            login: newAccount.login,
            sessionToken: newAccount.sessionToken
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

authRouter.post('/login/', async (req, res) => {
    try {
        const {login, password} = req.body;

        if (!(login?.trim() && password?.trim())) {
            res.status(400).send('Login and password are required');
            return;
        }

        const account = await AccountModel.findOne({login});

        if (!account) {
            res.status(403).send('User does not exist');
            return;
        }

        if (await bcrypt.compare(password, account.password)) {
            const token = crypto.randomBytes(64).toString('hex');
            account.sessionToken = token;

            await account.save();

            res.status(200).send({
                login: account.login,
                sessionToken: account.sessionToken
            });
        } else {
            res.status(403).send('Wrong login or password');
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

authRouter.get('/logout/:login/', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        account.sessionToken = '';
        await account.save();

        res.status(200).send();
    } catch (e) {
        res.status(500).send(e);
    }
});