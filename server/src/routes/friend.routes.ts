import { Router } from 'express';
import { socketMap } from '../index';
import { AccountModel } from '../models/account.model';
import { ChatModel } from '../models/chat.model';
import { InboxValueEnum } from '../enums/inbox-value.enum';
import { MessageModel } from '../models/message.model';
import { AccountInterface } from '../intefaces/account.interface';
import { Schema } from 'mongoose';

export const friendRouter = Router();

friendRouter.get('/searchFriends/', async (req, res) => {
    try {
        const query = req.query.login.toString();

        const users = await AccountModel.find({login: {$regex: query, $options: '$i'}}, 'login');

        res.status(200).send(users.map((value) => value.login));
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.get('/getFriendsInvites/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        res.status(200).send({
            pendingFriends: await getLoginsByIds(account.pendingFriends),
            sentFriends: await getLoginsByIds(account.sentFriends),
            friends: await getLoginsByIds(account.friends)
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getLoginsByIds(ids: Schema.Types.ObjectId[]): Promise<string[]> {
    const accounts = await AccountModel.find({_id: {$in: ids}}, 'login').exec();
    return accounts.map(({login}) => login);
}

friendRouter.put('/addFriend/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const addingLogin = req.body.login;

        if (login === addingLogin) {
            res.status(400).send('You can not add yourself');
            return
        }

        const addingAccount = await AccountModel.findOne({login: addingLogin});

        if (!addingAccount) {
            res.status(404).send('Account not found');
            return;
        }

        if (!account.friends.map((value) => value.toString()).includes(addingAccount._id.toString())) {
            if (account.sentFriends.map((value) => value.toString()).includes(addingAccount._id.toString()) &&
                addingAccount.pendingFriends.map((value) => value.toString()).includes(account._id.toString())) {
                account.sentFriends = account.sentFriends
                    .filter((value) => value.toString() !== addingAccount._id.toString());
                addingAccount.pendingFriends = addingAccount.pendingFriends
                    .filter((value) => value.toString() !== account._id.toString());

                account.friends.push(addingAccount._id);
                addingAccount.friends.push(account._id);

                if (!await ChatModel.findOne({participants: {$all: [account._id, addingAccount._id]}})) {
                    const newChat = new ChatModel({
                        participants: [account._id, addingAccount._id]
                    });

                    await newChat.save();
                    account.chats.push(newChat._id);
                    addingAccount.chats.push(newChat._id);
                }


            } else if (!(account.pendingFriends.map((value) => value.toString()).includes(addingAccount._id.toString()) &&
                addingAccount.sentFriends.map((value) => value.toString()).includes(account._id.toString()))) {
                account.pendingFriends.push(addingAccount._id);
                addingAccount.sentFriends.push(account._id);
            }
        }


        await account.save();
        await addingAccount.save();

        await socketMap.forEach(async (value, key) => {
            if (value === addingLogin) {
                key.send(JSON.stringify({
                    event: 'friends',
                    data: {
                        pendingFriends: await getLoginsByIds(addingAccount.pendingFriends),
                        sentFriends: await getLoginsByIds(addingAccount.sentFriends),
                        friends: await getLoginsByIds(addingAccount.friends)
                    }
                }));
            }
        });

        res.status(200).send({
            pendingFriends: await getLoginsByIds(account.pendingFriends),
            sentFriends: await getLoginsByIds(account.sentFriends),
            friends: await getLoginsByIds(account.friends)
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.put('/declineFriend/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const decliningLogin = req.body.login;

        if (login === decliningLogin) {
            res.status(400).send('You can not decline your own invite');
            return;
        }

        const decliningAccount = await AccountModel.findOne({login: decliningLogin});

        if (!decliningAccount) {
            res.status(404).send('Account not found');
            return;
        }

        account.sentFriends = account.sentFriends.filter((value) => value.toString() !== decliningAccount._id.toString());
        decliningAccount.pendingFriends = decliningAccount.pendingFriends.filter((value) => value.toString() !== account._id.toString());
        account.pendingFriends = account.pendingFriends.filter((value) => value.toString() !== decliningAccount._id.toString());
        decliningAccount.sentFriends = decliningAccount.sentFriends.filter((value) => value.toString() !== account._id.toString());

        await account.save();
        await decliningAccount.save();

        await socketMap.forEach(async (value, key) => {
            if (value === decliningLogin) {
                key.send(JSON.stringify({
                    event: 'friends',
                    data: {
                        pendingFriends: await getLoginsByIds(decliningAccount.pendingFriends),
                        sentFriends: await getLoginsByIds(decliningAccount.sentFriends),
                        friends: await getLoginsByIds(decliningAccount.friends)
                    }
                }));
            }
        });

        res.status(200).send({
            pendingFriends: await getLoginsByIds(account.pendingFriends),
            sentFriends: await getLoginsByIds(account.sentFriends),
            friends: await getLoginsByIds(account.friends)
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.put('/deleteFriend/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const deletingLogin = req.body.login;

        if (login === deletingLogin) {
            res.status(400).send('You can not delete yourself');
            return;
        }

        const deletingAccount = await AccountModel.findOne({login: deletingLogin});

        if (!deletingAccount) {
            res.status(404).send('Account not found');
            return;
        }

        account.friends = account.friends
            .filter((value) => value.toString() !== deletingAccount._id.toString());
        deletingAccount.friends = deletingAccount.friends
            .filter((value) => value.toString() !== account._id.toString());

        await account.save();
        await deletingAccount.save();

        await socketMap.forEach(async (value, key) => {
            if (value === deletingLogin) {
                key.send(JSON.stringify({
                    event: 'friends',
                    data: {
                        pendingFriends: await getLoginsByIds(deletingAccount.pendingFriends),
                        sentFriends: await getLoginsByIds(deletingAccount.sentFriends),
                        friends: await getLoginsByIds(deletingAccount.friends)
                    }
                }));
            }
        });

        res.status(200).send({
            pendingFriends: await getLoginsByIds(account.pendingFriends),
            sentFriends: await getLoginsByIds(account.sentFriends),
            friends: await getLoginsByIds(account.friends)
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.put('/addImportant/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const addingLogin = req.body.login;

        if (login === addingLogin) {
            res.status(400).send('You can not add yourself');
            return
        }

        const addingAccount = await AccountModel.findOne({login: addingLogin});

        if (!addingAccount) {
            res.status(404).send('Account not found');
            return;
        }

        if (!account.important.map((value) => value.toString()).includes(addingAccount._id.toString())) {
            account.important.push(addingAccount._id);
        }

        await account.save();

        res.status(200).send(await getLoginsByIds(account.important));
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.put('/deleteImportant/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const deletingLogin = req.body.login;

        if (login === deletingLogin) {
            res.status(400).send('You can not delete yourself');
            return;
        }

        const deletingAccount = await AccountModel.findOne({login: deletingLogin});

        if (!deletingAccount) {
            res.status(404).send('Account not found');
            return;
        }

        account.important = account.important.filter((value) => value.toString() !== deletingAccount._id.toString());

        await account.save();

        res.status(200).send(await getLoginsByIds(account.important));
    } catch (e) {
        res.status(500).send(e);
    }
});

friendRouter.get('/getValues/:login', async (req, res) => {
    try {
        const login = req.params.login;
        const token = req.headers.authorization.slice(7);
        const fields = req.query.fields ? req.query.fields.toString().split(',').map(value => +value) :
            [InboxValueEnum.ALL, InboxValueEnum.FRIENDS, InboxValueEnum.IMPORTANT, InboxValueEnum.UNREAD];

        const account = await AccountModel.findOne({login, sessionToken: token});

        if (!account) {
            res.status(401).send('Unauthorized');
            return;
        }

        const result = {};

        for (const value of fields) {
            result[value] = await getValueByField(account, value);
        }

        res.status(200).send(result);
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getValueByField(account: AccountInterface & { _id }, value: InboxValueEnum): Promise<number> {
    switch (value) {
        case InboxValueEnum.ALL:
            return account.chats.length;
        case InboxValueEnum.FRIENDS:
            return account.friends.length;
        case InboxValueEnum.IMPORTANT:
            return account.important.length;
        case InboxValueEnum.UNREAD:
            const chatsIds = account.chats;
            return await MessageModel.count({chatId: {$in: chatsIds}, read: false, ownerId: {$ne: account._id}});
        default:
            return 0;
    }
}
