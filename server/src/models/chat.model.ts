import {Schema, model} from 'mongoose';
import { ChatInterface } from '../intefaces/chat.interface';

const chatSchema = new Schema<ChatInterface>({
    participants: {type: [Schema.Types.ObjectId], required: true, default: [], ref: 'Account'},
    messages: {type: [Schema.Types.ObjectId], required: true, default: [], ref: 'Message'},
});

export const ChatModel = model<ChatInterface>('Chat', chatSchema);