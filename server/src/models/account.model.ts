import {AccountInterface} from '../intefaces/account.interface';
import {Schema, model} from 'mongoose';

export const accountSchema = new Schema<AccountInterface>({
    login: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    friends: {type: [Schema.Types.ObjectId], required: true, default: []},
    pendingFriends: {type: [Schema.Types.ObjectId], required: true, default: []},
    sentFriends: {type: [Schema.Types.ObjectId], required: true, default: []},
    important: {type: [Schema.Types.ObjectId], required: true, default: []},
    sessionToken: 'String',
    userInfo: {type: Schema.Types.ObjectId, ref: 'UserInfo'},
    messages: {type: [Schema.Types.ObjectId], ref: 'Message', required: true, default: []},
    chats: {type: [Schema.Types.ObjectId], ref: 'Chat', required: true, default: []}
});

export const AccountModel = model<AccountInterface>('Account', accountSchema);