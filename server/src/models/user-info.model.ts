import {Schema, model} from 'mongoose';
import { UserInfoInterface } from '../intefaces/user-info.interface';

const userInfoSchema = new Schema<UserInfoInterface>({
    name: 'String',
    surname: 'String',
    gender: 'Number',
    dateOfBirth: 'Number',
    phoneNumber: 'String',
    country: 'String',
    city: 'String',
    languages: {type: [String], default: []}
});

export const UserInfoModel = model<UserInfoInterface>('UserInfo', userInfoSchema);