import {Schema, model} from 'mongoose';
import { MessageInterface } from '../intefaces/message.interface';

const messageSchema = new Schema<MessageInterface>({
    chatId: {type: Schema.Types.ObjectId, required: true, ref: 'Chat'},
    ownerId: {type: Schema.Types.ObjectId, required: true, ref: 'Account'},
    date: {type: Number, required: true, default: +Date.now()},
    read: {type: Boolean, required: true, default: false},
    message: {type: String, required: true, default: ''}
});

export const MessageModel = model<MessageInterface>('Message', messageSchema);