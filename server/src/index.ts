import express, { Router } from 'express'
import expressWs from 'express-ws'
import dotenv from 'dotenv'
import bodyParser from 'body-parser'
import cors from 'cors'
import mongoose from 'mongoose'
import * as WebSocket from 'ws';

import { AccountModel } from './models/account.model';
import { authRouter } from './routes/auth.routes';
import { userRouter } from './routes/user.routes';
import { friendRouter } from './routes/friend.routes';
import { chatRouter } from './routes/chat.routes';
import { ChatModel } from './models/chat.model';
import { MessageModel } from './models/message.model';

dotenv.config();
const app = express();
const expressWsApp = expressWs(app);
const wsRouter = Router() as expressWs.Router;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const corsOptions = {
    origin: process.env.CLIENT_URL || 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions));
const port = process.env.PORT || 5000;


/**
 * establish connection to data base
 */
async function connectDb() {
    await mongoose.connect(process.env.DB_URL || 'mongodb://127.0.0.1:27017/chatDB');
}

connectDb().then(() => {
    const server = app.listen(port, () => {
        console.log(`Running on port ${port}`);
    });
    // server.on('upgrade', async(request, socket) => {
    //     const token = request.headers?.authorization?.slice(7) || '';
    //
    //     const account = await AccountModel.findOne({sessionToken: token});
    //
    //     if (!account) {
    //         socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
    //         socket.destroy();
    //         return;
    //     }
    // });
});

process.on('SIGINT', async () => {
    await mongoose.disconnect();
    console.log('connection terminated');
    process.exit();
});

app.get('/', (request, response) => {
    response.send('Hello world!');
});

app.use('/auth', authRouter);
app.use('/user', userRouter);
app.use('/friend', friendRouter);
app.use('/chat', chatRouter);

// WebSocket
export const socketMap = new Map<WebSocket, string>();
const wss = expressWsApp.getWss();

wsRouter.ws('/', async (ws) => {
    // console.log('Ws connected');

    ws.on('message', async (msg) => {
        try {
            const message = JSON.parse(JSON.parse(msg.toString()));
            const {event: msgEvent, data} = message;

            switch (msgEvent) {
                case 'connect': {
                    const {login, sessionToken} = data;
                    const account = await AccountModel.findOne({login, sessionToken});

                    if (!account) {
                        ws.close(1008, JSON.stringify({event: 'error', data: {code: 401, message: 'Unauthorized'}}));
                        break;
                    }

                    socketMap.set(ws, login);
                    // console.log(`auth: ${login}`);

                    await wss.clients.forEach(async (value) => {
                        const clientLogin = socketMap.get(value as WebSocket);
                        const clientAccount = await AccountModel.findOne({login: clientLogin});

                        if (!clientAccount) {
                            return;
                        }

                        if (account.friends.find((value1) => value1.toString() === clientAccount._id.toString())) {
                            value.send(JSON.stringify({event: 'online', data: {login, online: true}}));
                        }
                    });
                }
                    break;
                case 'message': {
                    const {chatId, message: sentMsg, owner: ownerLogin} = data;

                    if (ownerLogin !== socketMap.get(ws)) {
                        // console.log('not authorized');
                        ws.close(1008, JSON.stringify({event: 'error', data: {code: 401, message: 'Unauthorized'}}));
                        break;
                    }

                    const chat = await ChatModel.findById(chatId);
                    const owner = await AccountModel.findOne({login: ownerLogin});

                    if (!chat) {
                        ws.send(JSON.stringify({event: 'error', data: {code: 404, message: 'Chat not found'}}));
                        break;
                    }

                    const chatMemberId = chat.participants.find(value => value.toString() !== owner._id.toString());
                    const {login: chatMemberLogin} = await AccountModel.findById(chatMemberId);

                    const newMessage = new MessageModel({
                        chatId: chat._id,
                        ownerId: owner._id,
                        date: +Date.now(),
                        read: false,
                        message: sentMsg
                    });

                    await newMessage.save();

                    chat.messages.push(newMessage._id);
                    await chat.save();

                    await wss.clients.forEach(async (value) => {
                        const clientLogin = socketMap.get(value as WebSocket);
                        if (clientLogin === chatMemberLogin || clientLogin === owner.login) {
                            value.send(JSON.stringify({
                                event: 'message',
                                data: {
                                    chatId,
                                    message: {
                                        message: newMessage?.message,
                                        owner: newMessage?.ownerId.toString() === owner._id.toString() ? owner.login : chatMemberLogin,
                                        date: newMessage?.date,
                                        read: newMessage?.read
                                    }
                                }
                            }));
                        }
                    });
                }
                    break;
                case 'typing': {
                    const {chatId, typing, owner: ownerLogin} = data;

                    if (ownerLogin !== socketMap.get(ws)) {
                        // console.log('not authorized');
                        ws.close(1008, JSON.stringify({event: 'error', data: {code: 401, message: 'Unauthorized'}}));
                        break;
                    }

                    const chat = await ChatModel.findById(chatId);
                    const owner = await AccountModel.findOne({login: ownerLogin});

                    if (!chat) {
                        ws.send(JSON.stringify({event: 'error', data: {code: 404, message: 'Chat not found'}}));
                        break;
                    }

                    const chatMemberId = chat.participants.find(value => value.toString() !== owner._id.toString());
                    const {login: chatMemberLogin} = await AccountModel.findById(chatMemberId);

                    await wss.clients.forEach(async (value) => {
                        const clientLogin = socketMap.get(value as WebSocket);
                        if (clientLogin === chatMemberLogin) {
                            value.send(JSON.stringify({
                                event: 'typing',
                                data: {
                                    chatId,
                                    typing
                                }
                            }));
                        }
                    });
                }
                    break;
                case 'read': {
                    const {chatId, owner: ownerLogin} = data;

                    if (ownerLogin !== socketMap.get(ws)) {
                        // console.log('not authorized');
                        ws.close(1008, JSON.stringify({event: 'error', data: {code: 401, message: 'Unauthorized'}}));
                        break;
                    }

                    const chat = await ChatModel.findById(chatId);
                    const owner = await AccountModel.findOne({login: ownerLogin});

                    if (!chat) {
                        ws.send(JSON.stringify({event: 'error', data: {code: 404, message: 'Chat not found'}}));
                        break;
                    }

                    const chatMemberId = chat.participants.find(value => value.toString() !== owner._id.toString());
                    const {login: chatMemberLogin, _id: chatMemberIdObj} = await AccountModel.findById(chatMemberId);

                    const messages = await MessageModel.find({chatId, ownerId: chatMemberIdObj, read: false});
                    let isRead = false;
                    await messages.forEach(async (value) => {
                        value.read = true;
                        isRead = true;
                        await value.save();
                    });

                    if(!isRead) {
                        break;
                    }

                    wss.clients.forEach((client) => {
                        const clientLogin = socketMap.get(client as WebSocket);
                        if (clientLogin === chatMemberLogin) {
                            client.send(JSON.stringify({
                                event: 'read',
                                data: {
                                    chatId
                                }
                            }));
                        }
                    });
                }
                    break;
                default:
                    ws.send(JSON.stringify({event: 'error', data: {code: 404, message: 'event not found'}}));
            }
        } catch (e) {
            // console.log(e);
            return;
        }
    });

    ws.on('close', async () => {
        try {
            const login = socketMap.get(ws);
            const account = await AccountModel.findOne({login});

            if (!account) {
                return;
            }

            await wss.clients.forEach(async (value) => {
                const clientLogin = socketMap.get(value as WebSocket);
                const clientAccount = await AccountModel.findOne({login: clientLogin});

                if (!clientAccount) {
                    return;
                }

                if (account.friends.find((value1) => value1.toString() === clientAccount._id.toString())) {
                    value.send(JSON.stringify({event: 'online', data: {login, online: false}}));
                }
            });

            // console.log('WebSocket was closed');
        } catch (e) {
            console.log(`close error: ${e.message}`);
        } finally {
            socketMap.delete(ws);
        }
    })
});

app.use('/ws', wsRouter);