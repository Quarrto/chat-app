import { Schema } from "mongoose";

export interface MessageInterface {
    chatId: Schema.Types.ObjectId;
    ownerId: Schema.Types.ObjectId;
    date: number;
    read: boolean;
    message: string
}