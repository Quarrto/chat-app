import { Schema } from "mongoose";

export interface ChatInterface {
    participants: Schema.Types.ObjectId[];
    messages: Schema.Types.ObjectId[];
}