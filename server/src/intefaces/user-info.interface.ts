import { GenderEnum } from '../enums/gender.enum';

export interface UserInfoInterface {
    name?: string;
    surname?: string;
    gender?: GenderEnum;
    dateOfBirth?: number;
    phoneNumber?: string;
    country?: string;
    city?: string;
    languages?: string[];
}