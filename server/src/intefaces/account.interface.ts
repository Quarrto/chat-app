import { Schema } from 'mongoose';

export interface AccountInterface {
    login: string;
    password: string;
    friends: Schema.Types.ObjectId[];
    pendingFriends: Schema.Types.ObjectId[];
    sentFriends: Schema.Types.ObjectId[];
    important: Schema.Types.ObjectId[];
    sessionToken?: string;
    userInfo: Schema.Types.ObjectId;
    messages: Schema.Types.ObjectId[];
    chats: Schema.Types.ObjectId[];
}