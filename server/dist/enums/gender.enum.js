"use strict";
exports.__esModule = true;
exports.GenderEnum = void 0;
var GenderEnum;
(function (GenderEnum) {
    GenderEnum[GenderEnum["UNMARKED"] = 0] = "UNMARKED";
    GenderEnum[GenderEnum["MALE"] = 1] = "MALE";
    GenderEnum[GenderEnum["FEMALE"] = 2] = "FEMALE";
})(GenderEnum = exports.GenderEnum || (exports.GenderEnum = {}));
//# sourceMappingURL=gender.enum.js.map