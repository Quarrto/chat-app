"use strict";
exports.__esModule = true;
exports.InboxValueEnum = void 0;
var InboxValueEnum;
(function (InboxValueEnum) {
    InboxValueEnum[InboxValueEnum["ALL"] = 0] = "ALL";
    InboxValueEnum[InboxValueEnum["FRIENDS"] = 1] = "FRIENDS";
    InboxValueEnum[InboxValueEnum["UNREAD"] = 2] = "UNREAD";
    InboxValueEnum[InboxValueEnum["IMPORTANT"] = 3] = "IMPORTANT";
})(InboxValueEnum = exports.InboxValueEnum || (exports.InboxValueEnum = {}));
//# sourceMappingURL=inbox-value.enum.js.map