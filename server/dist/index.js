"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.socketMap = void 0;
var express_1 = __importStar(require("express"));
var express_ws_1 = __importDefault(require("express-ws"));
var dotenv_1 = __importDefault(require("dotenv"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var mongoose_1 = __importDefault(require("mongoose"));
var account_model_1 = require("./models/account.model");
var auth_routes_1 = require("./routes/auth.routes");
var user_routes_1 = require("./routes/user.routes");
var friend_routes_1 = require("./routes/friend.routes");
var chat_routes_1 = require("./routes/chat.routes");
var chat_model_1 = require("./models/chat.model");
var message_model_1 = require("./models/message.model");
dotenv_1["default"].config();
var app = (0, express_1["default"])();
var expressWsApp = (0, express_ws_1["default"])(app);
var wsRouter = (0, express_1.Router)();
app.use(body_parser_1["default"].urlencoded({ extended: false }));
app.use(body_parser_1["default"].json());
var corsOptions = {
    origin: process.env.CLIENT_URL || 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use((0, cors_1["default"])(corsOptions));
var port = process.env.PORT || 5000;
/**
 * establish connection to data base
 */
function connectDb() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mongoose_1["default"].connect(process.env.DB_URL || 'mongodb://127.0.0.1:27017/chatDB')];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
connectDb().then(function () {
    var server = app.listen(port, function () {
        console.log("Running on port " + port);
    });
    // server.on('upgrade', async(request, socket) => {
    //     const token = request.headers?.authorization?.slice(7) || '';
    //
    //     const account = await AccountModel.findOne({sessionToken: token});
    //
    //     if (!account) {
    //         socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
    //         socket.destroy();
    //         return;
    //     }
    // });
});
process.on('SIGINT', function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, mongoose_1["default"].disconnect()];
            case 1:
                _a.sent();
                console.log('connection terminated');
                process.exit();
                return [2 /*return*/];
        }
    });
}); });
app.get('/', function (request, response) {
    response.send('Hello world!');
});
app.use('/auth', auth_routes_1.authRouter);
app.use('/user', user_routes_1.userRouter);
app.use('/friend', friend_routes_1.friendRouter);
app.use('/chat', chat_routes_1.chatRouter);
// WebSocket
exports.socketMap = new Map();
var wss = expressWsApp.getWss();
wsRouter.ws('/', function (ws) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        // console.log('Ws connected');
        ws.on('message', function (msg) { return __awaiter(void 0, void 0, void 0, function () {
            var message, msgEvent, data, _a, login_1, sessionToken, account_1, chatId_1, sentMsg, ownerLogin, chat, owner_1, chatMemberId, chatMemberLogin_1, newMessage_1, chatId_2, typing_1, ownerLogin, chat, owner_2, chatMemberId, chatMemberLogin_2, chatId_3, ownerLogin, chat, owner_3, chatMemberId, _b, chatMemberLogin_3, chatMemberIdObj, messages, isRead_1, e_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 24, , 25]);
                        message = JSON.parse(JSON.parse(msg.toString()));
                        msgEvent = message.event, data = message.data;
                        _a = msgEvent;
                        switch (_a) {
                            case 'connect': return [3 /*break*/, 1];
                            case 'message': return [3 /*break*/, 4];
                            case 'typing': return [3 /*break*/, 11];
                            case 'read': return [3 /*break*/, 16];
                        }
                        return [3 /*break*/, 22];
                    case 1:
                        login_1 = data.login, sessionToken = data.sessionToken;
                        return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login_1, sessionToken: sessionToken })];
                    case 2:
                        account_1 = _c.sent();
                        if (!account_1) {
                            ws.close(1008, JSON.stringify({ event: 'error', data: { code: 401, message: 'Unauthorized' } }));
                            return [3 /*break*/, 23];
                        }
                        exports.socketMap.set(ws, login_1);
                        // console.log(`auth: ${login}`);
                        return [4 /*yield*/, wss.clients.forEach(function (value) { return __awaiter(void 0, void 0, void 0, function () {
                                var clientLogin, clientAccount;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            clientLogin = exports.socketMap.get(value);
                                            return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: clientLogin })];
                                        case 1:
                                            clientAccount = _a.sent();
                                            if (!clientAccount) {
                                                return [2 /*return*/];
                                            }
                                            if (account_1.friends.find(function (value1) { return value1.toString() === clientAccount._id.toString(); })) {
                                                value.send(JSON.stringify({ event: 'online', data: { login: login_1, online: true } }));
                                            }
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 3:
                        // console.log(`auth: ${login}`);
                        _c.sent();
                        return [3 /*break*/, 23];
                    case 4:
                        chatId_1 = data.chatId, sentMsg = data.message, ownerLogin = data.owner;
                        if (ownerLogin !== exports.socketMap.get(ws)) {
                            // console.log('not authorized');
                            ws.close(1008, JSON.stringify({ event: 'error', data: { code: 401, message: 'Unauthorized' } }));
                            return [3 /*break*/, 23];
                        }
                        return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId_1)];
                    case 5:
                        chat = _c.sent();
                        return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: ownerLogin })];
                    case 6:
                        owner_1 = _c.sent();
                        if (!chat) {
                            ws.send(JSON.stringify({ event: 'error', data: { code: 404, message: 'Chat not found' } }));
                            return [3 /*break*/, 23];
                        }
                        chatMemberId = chat.participants.find(function (value) { return value.toString() !== owner_1._id.toString(); });
                        return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
                    case 7:
                        chatMemberLogin_1 = (_c.sent()).login;
                        newMessage_1 = new message_model_1.MessageModel({
                            chatId: chat._id,
                            ownerId: owner_1._id,
                            date: +Date.now(),
                            read: false,
                            message: sentMsg
                        });
                        return [4 /*yield*/, newMessage_1.save()];
                    case 8:
                        _c.sent();
                        chat.messages.push(newMessage_1._id);
                        return [4 /*yield*/, chat.save()];
                    case 9:
                        _c.sent();
                        return [4 /*yield*/, wss.clients.forEach(function (value) { return __awaiter(void 0, void 0, void 0, function () {
                                var clientLogin;
                                return __generator(this, function (_a) {
                                    clientLogin = exports.socketMap.get(value);
                                    if (clientLogin === chatMemberLogin_1 || clientLogin === owner_1.login) {
                                        value.send(JSON.stringify({
                                            event: 'message',
                                            data: {
                                                chatId: chatId_1,
                                                message: {
                                                    message: newMessage_1 === null || newMessage_1 === void 0 ? void 0 : newMessage_1.message,
                                                    owner: (newMessage_1 === null || newMessage_1 === void 0 ? void 0 : newMessage_1.ownerId.toString()) === owner_1._id.toString() ? owner_1.login : chatMemberLogin_1,
                                                    date: newMessage_1 === null || newMessage_1 === void 0 ? void 0 : newMessage_1.date,
                                                    read: newMessage_1 === null || newMessage_1 === void 0 ? void 0 : newMessage_1.read
                                                }
                                            }
                                        }));
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 10:
                        _c.sent();
                        return [3 /*break*/, 23];
                    case 11:
                        chatId_2 = data.chatId, typing_1 = data.typing, ownerLogin = data.owner;
                        if (ownerLogin !== exports.socketMap.get(ws)) {
                            // console.log('not authorized');
                            ws.close(1008, JSON.stringify({ event: 'error', data: { code: 401, message: 'Unauthorized' } }));
                            return [3 /*break*/, 23];
                        }
                        return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId_2)];
                    case 12:
                        chat = _c.sent();
                        return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: ownerLogin })];
                    case 13:
                        owner_2 = _c.sent();
                        if (!chat) {
                            ws.send(JSON.stringify({ event: 'error', data: { code: 404, message: 'Chat not found' } }));
                            return [3 /*break*/, 23];
                        }
                        chatMemberId = chat.participants.find(function (value) { return value.toString() !== owner_2._id.toString(); });
                        return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
                    case 14:
                        chatMemberLogin_2 = (_c.sent()).login;
                        return [4 /*yield*/, wss.clients.forEach(function (value) { return __awaiter(void 0, void 0, void 0, function () {
                                var clientLogin;
                                return __generator(this, function (_a) {
                                    clientLogin = exports.socketMap.get(value);
                                    if (clientLogin === chatMemberLogin_2) {
                                        value.send(JSON.stringify({
                                            event: 'typing',
                                            data: {
                                                chatId: chatId_2,
                                                typing: typing_1
                                            }
                                        }));
                                    }
                                    return [2 /*return*/];
                                });
                            }); })];
                    case 15:
                        _c.sent();
                        return [3 /*break*/, 23];
                    case 16:
                        chatId_3 = data.chatId, ownerLogin = data.owner;
                        if (ownerLogin !== exports.socketMap.get(ws)) {
                            // console.log('not authorized');
                            ws.close(1008, JSON.stringify({ event: 'error', data: { code: 401, message: 'Unauthorized' } }));
                            return [3 /*break*/, 23];
                        }
                        return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId_3)];
                    case 17:
                        chat = _c.sent();
                        return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: ownerLogin })];
                    case 18:
                        owner_3 = _c.sent();
                        if (!chat) {
                            ws.send(JSON.stringify({ event: 'error', data: { code: 404, message: 'Chat not found' } }));
                            return [3 /*break*/, 23];
                        }
                        chatMemberId = chat.participants.find(function (value) { return value.toString() !== owner_3._id.toString(); });
                        return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
                    case 19:
                        _b = _c.sent(), chatMemberLogin_3 = _b.login, chatMemberIdObj = _b._id;
                        return [4 /*yield*/, message_model_1.MessageModel.find({ chatId: chatId_3, ownerId: chatMemberIdObj, read: false })];
                    case 20:
                        messages = _c.sent();
                        isRead_1 = false;
                        return [4 /*yield*/, messages.forEach(function (value) { return __awaiter(void 0, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            value.read = true;
                                            isRead_1 = true;
                                            return [4 /*yield*/, value.save()];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 21:
                        _c.sent();
                        if (!isRead_1) {
                            return [3 /*break*/, 23];
                        }
                        wss.clients.forEach(function (client) {
                            var clientLogin = exports.socketMap.get(client);
                            if (clientLogin === chatMemberLogin_3) {
                                client.send(JSON.stringify({
                                    event: 'read',
                                    data: {
                                        chatId: chatId_3
                                    }
                                }));
                            }
                        });
                        return [3 /*break*/, 23];
                    case 22:
                        ws.send(JSON.stringify({ event: 'error', data: { code: 404, message: 'event not found' } }));
                        _c.label = 23;
                    case 23: return [3 /*break*/, 25];
                    case 24:
                        e_1 = _c.sent();
                        // console.log(e);
                        return [2 /*return*/];
                    case 25: return [2 /*return*/];
                }
            });
        }); });
        ws.on('close', function () { return __awaiter(void 0, void 0, void 0, function () {
            var login_2, account_2, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, 4, 5]);
                        login_2 = exports.socketMap.get(ws);
                        return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login_2 })];
                    case 1:
                        account_2 = _a.sent();
                        if (!account_2) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, wss.clients.forEach(function (value) { return __awaiter(void 0, void 0, void 0, function () {
                                var clientLogin, clientAccount;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            clientLogin = exports.socketMap.get(value);
                                            return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: clientLogin })];
                                        case 1:
                                            clientAccount = _a.sent();
                                            if (!clientAccount) {
                                                return [2 /*return*/];
                                            }
                                            if (account_2.friends.find(function (value1) { return value1.toString() === clientAccount._id.toString(); })) {
                                                value.send(JSON.stringify({ event: 'online', data: { login: login_2, online: false } }));
                                            }
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        e_2 = _a.sent();
                        console.log("close error: " + e_2.message);
                        return [3 /*break*/, 5];
                    case 4:
                        exports.socketMap["delete"](ws);
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        }); });
        return [2 /*return*/];
    });
}); });
app.use('/ws', wsRouter);
//# sourceMappingURL=index.js.map