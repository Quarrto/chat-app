"use strict";
exports.__esModule = true;
exports.MessageModel = void 0;
var mongoose_1 = require("mongoose");
var messageSchema = new mongoose_1.Schema({
    chatId: { type: mongoose_1.Schema.Types.ObjectId, required: true, ref: 'Chat' },
    ownerId: { type: mongoose_1.Schema.Types.ObjectId, required: true, ref: 'Account' },
    date: { type: Number, required: true, "default": +Date.now() },
    read: { type: Boolean, required: true, "default": false },
    message: { type: String, required: true, "default": '' }
});
exports.MessageModel = (0, mongoose_1.model)('Message', messageSchema);
//# sourceMappingURL=message.model.js.map