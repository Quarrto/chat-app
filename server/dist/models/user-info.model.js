"use strict";
exports.__esModule = true;
exports.UserInfoModel = void 0;
var mongoose_1 = require("mongoose");
var userInfoSchema = new mongoose_1.Schema({
    name: 'String',
    surname: 'String',
    gender: 'Number',
    dateOfBirth: 'Number',
    phoneNumber: 'String',
    country: 'String',
    city: 'String',
    languages: { type: [String], "default": [] }
});
exports.UserInfoModel = (0, mongoose_1.model)('UserInfo', userInfoSchema);
//# sourceMappingURL=user-info.model.js.map