"use strict";
exports.__esModule = true;
exports.AccountModel = exports.accountSchema = void 0;
var mongoose_1 = require("mongoose");
exports.accountSchema = new mongoose_1.Schema({
    login: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    friends: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [] },
    pendingFriends: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [] },
    sentFriends: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [] },
    important: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [] },
    sessionToken: 'String',
    userInfo: { type: mongoose_1.Schema.Types.ObjectId, ref: 'UserInfo' },
    messages: { type: [mongoose_1.Schema.Types.ObjectId], ref: 'Message', required: true, "default": [] },
    chats: { type: [mongoose_1.Schema.Types.ObjectId], ref: 'Chat', required: true, "default": [] }
});
exports.AccountModel = (0, mongoose_1.model)('Account', exports.accountSchema);
//# sourceMappingURL=account.model.js.map