"use strict";
exports.__esModule = true;
exports.ChatModel = void 0;
var mongoose_1 = require("mongoose");
var chatSchema = new mongoose_1.Schema({
    participants: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [], ref: 'Account' },
    messages: { type: [mongoose_1.Schema.Types.ObjectId], required: true, "default": [], ref: 'Message' }
});
exports.ChatModel = (0, mongoose_1.model)('Chat', chatSchema);
//# sourceMappingURL=chat.model.js.map