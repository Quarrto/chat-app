"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.friendRouter = void 0;
var express_1 = require("express");
var index_1 = require("../index");
var account_model_1 = require("../models/account.model");
var chat_model_1 = require("../models/chat.model");
var inbox_value_enum_1 = require("../enums/inbox-value.enum");
var message_model_1 = require("../models/message.model");
exports.friendRouter = (0, express_1.Router)();
exports.friendRouter.get('/searchFriends/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var query, users, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                query = req.query.login.toString();
                return [4 /*yield*/, account_model_1.AccountModel.find({ login: { $regex: query, $options: '$i' } }, 'login')];
            case 1:
                users = _a.sent();
                res.status(200).send(users.map(function (value) { return value.login; }));
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                res.status(500).send(e_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.get('/getFriendsInvites/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, _a, _b, e_2;
    var _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 5, , 6]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _d.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                _b = (_a = res.status(200)).send;
                _c = {};
                return [4 /*yield*/, getLoginsByIds(account.pendingFriends)];
            case 2:
                _c.pendingFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account.sentFriends)];
            case 3:
                _c.sentFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account.friends)];
            case 4:
                _b.apply(_a, [(_c.friends = _d.sent(),
                        _c)]);
                return [3 /*break*/, 6];
            case 5:
                e_2 = _d.sent();
                res.status(500).send(e_2);
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
function getLoginsByIds(ids) {
    return __awaiter(this, void 0, void 0, function () {
        var accounts;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, account_model_1.AccountModel.find({ _id: { $in: ids } }, 'login').exec()];
                case 1:
                    accounts = _a.sent();
                    return [2 /*return*/, accounts.map(function (_a) {
                            var login = _a.login;
                            return login;
                        })];
            }
        });
    });
}
exports.friendRouter.put('/addFriend/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account_1, addingLogin_1, addingAccount_1, newChat, _a, _b, e_3;
    var _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 14, , 15]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account_1 = _d.sent();
                if (!account_1) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                addingLogin_1 = req.body.login;
                if (login === addingLogin_1) {
                    res.status(400).send('You can not add yourself');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: addingLogin_1 })];
            case 2:
                addingAccount_1 = _d.sent();
                if (!addingAccount_1) {
                    res.status(404).send('Account not found');
                    return [2 /*return*/];
                }
                if (!!account_1.friends.map(function (value) { return value.toString(); }).includes(addingAccount_1._id.toString())) return [3 /*break*/, 7];
                if (!(account_1.sentFriends.map(function (value) { return value.toString(); }).includes(addingAccount_1._id.toString()) &&
                    addingAccount_1.pendingFriends.map(function (value) { return value.toString(); }).includes(account_1._id.toString()))) return [3 /*break*/, 6];
                account_1.sentFriends = account_1.sentFriends
                    .filter(function (value) { return value.toString() !== addingAccount_1._id.toString(); });
                addingAccount_1.pendingFriends = addingAccount_1.pendingFriends
                    .filter(function (value) { return value.toString() !== account_1._id.toString(); });
                account_1.friends.push(addingAccount_1._id);
                addingAccount_1.friends.push(account_1._id);
                return [4 /*yield*/, chat_model_1.ChatModel.findOne({ participants: { $all: [account_1._id, addingAccount_1._id] } })];
            case 3:
                if (!!(_d.sent())) return [3 /*break*/, 5];
                newChat = new chat_model_1.ChatModel({
                    participants: [account_1._id, addingAccount_1._id]
                });
                return [4 /*yield*/, newChat.save()];
            case 4:
                _d.sent();
                account_1.chats.push(newChat._id);
                addingAccount_1.chats.push(newChat._id);
                _d.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                if (!(account_1.pendingFriends.map(function (value) { return value.toString(); }).includes(addingAccount_1._id.toString()) &&
                    addingAccount_1.sentFriends.map(function (value) { return value.toString(); }).includes(account_1._id.toString()))) {
                    account_1.pendingFriends.push(addingAccount_1._id);
                    addingAccount_1.sentFriends.push(account_1._id);
                }
                _d.label = 7;
            case 7: return [4 /*yield*/, account_1.save()];
            case 8:
                _d.sent();
                return [4 /*yield*/, addingAccount_1.save()];
            case 9:
                _d.sent();
                return [4 /*yield*/, index_1.socketMap.forEach(function (value, key) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a, _b, _c, _d;
                        var _e, _f;
                        return __generator(this, function (_g) {
                            switch (_g.label) {
                                case 0:
                                    if (!(value === addingLogin_1)) return [3 /*break*/, 4];
                                    _b = (_a = key).send;
                                    _d = (_c = JSON).stringify;
                                    _e = {
                                        event: 'friends'
                                    };
                                    _f = {};
                                    return [4 /*yield*/, getLoginsByIds(addingAccount_1.pendingFriends)];
                                case 1:
                                    _f.pendingFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(addingAccount_1.sentFriends)];
                                case 2:
                                    _f.sentFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(addingAccount_1.friends)];
                                case 3:
                                    _b.apply(_a, [_d.apply(_c, [(_e.data = (_f.friends = _g.sent(),
                                                _f),
                                                _e)])]);
                                    _g.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 10:
                _d.sent();
                _b = (_a = res.status(200)).send;
                _c = {};
                return [4 /*yield*/, getLoginsByIds(account_1.pendingFriends)];
            case 11:
                _c.pendingFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_1.sentFriends)];
            case 12:
                _c.sentFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_1.friends)];
            case 13:
                _b.apply(_a, [(_c.friends = _d.sent(),
                        _c)]);
                return [3 /*break*/, 15];
            case 14:
                e_3 = _d.sent();
                res.status(500).send(e_3);
                return [3 /*break*/, 15];
            case 15: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.put('/declineFriend/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account_2, decliningLogin_1, decliningAccount_1, _a, _b, e_4;
    var _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 9, , 10]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account_2 = _d.sent();
                if (!account_2) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                decliningLogin_1 = req.body.login;
                if (login === decliningLogin_1) {
                    res.status(400).send('You can not decline your own invite');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: decliningLogin_1 })];
            case 2:
                decliningAccount_1 = _d.sent();
                if (!decliningAccount_1) {
                    res.status(404).send('Account not found');
                    return [2 /*return*/];
                }
                account_2.sentFriends = account_2.sentFriends.filter(function (value) { return value.toString() !== decliningAccount_1._id.toString(); });
                decliningAccount_1.pendingFriends = decliningAccount_1.pendingFriends.filter(function (value) { return value.toString() !== account_2._id.toString(); });
                account_2.pendingFriends = account_2.pendingFriends.filter(function (value) { return value.toString() !== decliningAccount_1._id.toString(); });
                decliningAccount_1.sentFriends = decliningAccount_1.sentFriends.filter(function (value) { return value.toString() !== account_2._id.toString(); });
                return [4 /*yield*/, account_2.save()];
            case 3:
                _d.sent();
                return [4 /*yield*/, decliningAccount_1.save()];
            case 4:
                _d.sent();
                return [4 /*yield*/, index_1.socketMap.forEach(function (value, key) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a, _b, _c, _d;
                        var _e, _f;
                        return __generator(this, function (_g) {
                            switch (_g.label) {
                                case 0:
                                    if (!(value === decliningLogin_1)) return [3 /*break*/, 4];
                                    _b = (_a = key).send;
                                    _d = (_c = JSON).stringify;
                                    _e = {
                                        event: 'friends'
                                    };
                                    _f = {};
                                    return [4 /*yield*/, getLoginsByIds(decliningAccount_1.pendingFriends)];
                                case 1:
                                    _f.pendingFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(decliningAccount_1.sentFriends)];
                                case 2:
                                    _f.sentFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(decliningAccount_1.friends)];
                                case 3:
                                    _b.apply(_a, [_d.apply(_c, [(_e.data = (_f.friends = _g.sent(),
                                                _f),
                                                _e)])]);
                                    _g.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 5:
                _d.sent();
                _b = (_a = res.status(200)).send;
                _c = {};
                return [4 /*yield*/, getLoginsByIds(account_2.pendingFriends)];
            case 6:
                _c.pendingFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_2.sentFriends)];
            case 7:
                _c.sentFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_2.friends)];
            case 8:
                _b.apply(_a, [(_c.friends = _d.sent(),
                        _c)]);
                return [3 /*break*/, 10];
            case 9:
                e_4 = _d.sent();
                res.status(500).send(e_4);
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.put('/deleteFriend/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account_3, deletingLogin_1, deletingAccount_1, _a, _b, e_5;
    var _c;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 9, , 10]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account_3 = _d.sent();
                if (!account_3) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                deletingLogin_1 = req.body.login;
                if (login === deletingLogin_1) {
                    res.status(400).send('You can not delete yourself');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: deletingLogin_1 })];
            case 2:
                deletingAccount_1 = _d.sent();
                if (!deletingAccount_1) {
                    res.status(404).send('Account not found');
                    return [2 /*return*/];
                }
                account_3.friends = account_3.friends
                    .filter(function (value) { return value.toString() !== deletingAccount_1._id.toString(); });
                deletingAccount_1.friends = deletingAccount_1.friends
                    .filter(function (value) { return value.toString() !== account_3._id.toString(); });
                return [4 /*yield*/, account_3.save()];
            case 3:
                _d.sent();
                return [4 /*yield*/, deletingAccount_1.save()];
            case 4:
                _d.sent();
                return [4 /*yield*/, index_1.socketMap.forEach(function (value, key) { return __awaiter(void 0, void 0, void 0, function () {
                        var _a, _b, _c, _d;
                        var _e, _f;
                        return __generator(this, function (_g) {
                            switch (_g.label) {
                                case 0:
                                    if (!(value === deletingLogin_1)) return [3 /*break*/, 4];
                                    _b = (_a = key).send;
                                    _d = (_c = JSON).stringify;
                                    _e = {
                                        event: 'friends'
                                    };
                                    _f = {};
                                    return [4 /*yield*/, getLoginsByIds(deletingAccount_1.pendingFriends)];
                                case 1:
                                    _f.pendingFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(deletingAccount_1.sentFriends)];
                                case 2:
                                    _f.sentFriends = _g.sent();
                                    return [4 /*yield*/, getLoginsByIds(deletingAccount_1.friends)];
                                case 3:
                                    _b.apply(_a, [_d.apply(_c, [(_e.data = (_f.friends = _g.sent(),
                                                _f),
                                                _e)])]);
                                    _g.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); })];
            case 5:
                _d.sent();
                _b = (_a = res.status(200)).send;
                _c = {};
                return [4 /*yield*/, getLoginsByIds(account_3.pendingFriends)];
            case 6:
                _c.pendingFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_3.sentFriends)];
            case 7:
                _c.sentFriends = _d.sent();
                return [4 /*yield*/, getLoginsByIds(account_3.friends)];
            case 8:
                _b.apply(_a, [(_c.friends = _d.sent(),
                        _c)]);
                return [3 /*break*/, 10];
            case 9:
                e_5 = _d.sent();
                res.status(500).send(e_5);
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.put('/addImportant/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, addingLogin, addingAccount, _a, _b, e_6;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 5, , 6]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _c.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                addingLogin = req.body.login;
                if (login === addingLogin) {
                    res.status(400).send('You can not add yourself');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: addingLogin })];
            case 2:
                addingAccount = _c.sent();
                if (!addingAccount) {
                    res.status(404).send('Account not found');
                    return [2 /*return*/];
                }
                if (!account.important.map(function (value) { return value.toString(); }).includes(addingAccount._id.toString())) {
                    account.important.push(addingAccount._id);
                }
                return [4 /*yield*/, account.save()];
            case 3:
                _c.sent();
                _b = (_a = res.status(200)).send;
                return [4 /*yield*/, getLoginsByIds(account.important)];
            case 4:
                _b.apply(_a, [_c.sent()]);
                return [3 /*break*/, 6];
            case 5:
                e_6 = _c.sent();
                res.status(500).send(e_6);
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.put('/deleteImportant/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, deletingLogin, deletingAccount_2, _a, _b, e_7;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 5, , 6]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _c.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                deletingLogin = req.body.login;
                if (login === deletingLogin) {
                    res.status(400).send('You can not delete yourself');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: deletingLogin })];
            case 2:
                deletingAccount_2 = _c.sent();
                if (!deletingAccount_2) {
                    res.status(404).send('Account not found');
                    return [2 /*return*/];
                }
                account.important = account.important.filter(function (value) { return value.toString() !== deletingAccount_2._id.toString(); });
                return [4 /*yield*/, account.save()];
            case 3:
                _c.sent();
                _b = (_a = res.status(200)).send;
                return [4 /*yield*/, getLoginsByIds(account.important)];
            case 4:
                _b.apply(_a, [_c.sent()]);
                return [3 /*break*/, 6];
            case 5:
                e_7 = _c.sent();
                res.status(500).send(e_7);
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
exports.friendRouter.get('/getValues/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, fields, account, result, _i, fields_1, value, _a, _b, e_8;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 6, , 7]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                fields = req.query.fields ? req.query.fields.toString().split(',').map(function (value) { return +value; }) :
                    [inbox_value_enum_1.InboxValueEnum.ALL, inbox_value_enum_1.InboxValueEnum.FRIENDS, inbox_value_enum_1.InboxValueEnum.IMPORTANT, inbox_value_enum_1.InboxValueEnum.UNREAD];
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _c.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                result = {};
                _i = 0, fields_1 = fields;
                _c.label = 2;
            case 2:
                if (!(_i < fields_1.length)) return [3 /*break*/, 5];
                value = fields_1[_i];
                _a = result;
                _b = value;
                return [4 /*yield*/, getValueByField(account, value)];
            case 3:
                _a[_b] = _c.sent();
                _c.label = 4;
            case 4:
                _i++;
                return [3 /*break*/, 2];
            case 5:
                res.status(200).send(result);
                return [3 /*break*/, 7];
            case 6:
                e_8 = _c.sent();
                res.status(500).send(e_8);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
function getValueByField(account, value) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, chatsIds;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = value;
                    switch (_a) {
                        case inbox_value_enum_1.InboxValueEnum.ALL: return [3 /*break*/, 1];
                        case inbox_value_enum_1.InboxValueEnum.FRIENDS: return [3 /*break*/, 2];
                        case inbox_value_enum_1.InboxValueEnum.IMPORTANT: return [3 /*break*/, 3];
                        case inbox_value_enum_1.InboxValueEnum.UNREAD: return [3 /*break*/, 4];
                    }
                    return [3 /*break*/, 6];
                case 1: return [2 /*return*/, account.chats.length];
                case 2: return [2 /*return*/, account.friends.length];
                case 3: return [2 /*return*/, account.important.length];
                case 4:
                    chatsIds = account.chats;
                    return [4 /*yield*/, message_model_1.MessageModel.count({ chatId: { $in: chatsIds }, read: false, ownerId: { $ne: account._id } })];
                case 5: return [2 /*return*/, _b.sent()];
                case 6: return [2 /*return*/, 0];
            }
        });
    });
}
//# sourceMappingURL=friend.routes.js.map