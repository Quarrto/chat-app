"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.userRouter = void 0;
var express_1 = require("express");
var account_model_1 = require("../models/account.model");
var user_info_model_1 = require("../models/user-info.model");
exports.userRouter = (0, express_1.Router)();
exports.userRouter.put('/setUserInfo/:login/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, _a, name_1, surname, gender, dateOfBirth, phoneNumber, country, city, languages, userInfo, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 3, , 4]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _b.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                _a = req.body, name_1 = _a.name, surname = _a.surname, gender = _a.gender, dateOfBirth = _a.dateOfBirth, phoneNumber = _a.phoneNumber, country = _a.country, city = _a.city, languages = _a.languages;
                return [4 /*yield*/, user_info_model_1.UserInfoModel.findByIdAndUpdate(account.userInfo, {
                        name: name_1,
                        surname: surname,
                        gender: gender,
                        dateOfBirth: dateOfBirth,
                        phoneNumber: phoneNumber,
                        country: country,
                        city: city,
                        languages: languages
                    }, {
                        projection: ['-_id', '-__v'],
                        "new": true
                    })];
            case 2:
                userInfo = _b.sent();
                res.status(200).send(userInfo);
                return [3 /*break*/, 4];
            case 3:
                e_1 = _b.sent();
                res.status(500).send(e_1);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
exports.userRouter.get('/getUserInfo/:login/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, infoAccount, userInfo, e_2;
    var _a, _b, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                _e.trys.push([0, 4, , 5]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ sessionToken: token })];
            case 1:
                account = _e.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login })];
            case 2:
                infoAccount = _e.sent();
                if (!infoAccount) {
                    res.status(404).send('User not found');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, user_info_model_1.UserInfoModel.findOne({ _id: infoAccount.userInfo }).exec()];
            case 3:
                userInfo = _e.sent();
                res.status(200).send({
                    name: userInfo.name,
                    surname: userInfo.surname,
                    gender: userInfo.gender,
                    dateOfBirth: userInfo.dateOfBirth,
                    phoneNumber: userInfo.phoneNumber,
                    country: userInfo.country,
                    city: userInfo.city,
                    languages: userInfo.languages,
                    isImportant: ((_a = account.important) === null || _a === void 0 ? void 0 : _a.includes(infoAccount._id)) || false,
                    isFriend: ((_b = account.friends) === null || _b === void 0 ? void 0 : _b.includes(infoAccount._id)) || false,
                    isPendingFriend: ((_c = account.pendingFriends) === null || _c === void 0 ? void 0 : _c.includes(infoAccount._id)) || false,
                    isSentFriend: ((_d = account.sentFriends) === null || _d === void 0 ? void 0 : _d.includes(infoAccount._id)) || false
                });
                return [3 /*break*/, 5];
            case 4:
                e_2 = _e.sent();
                res.status(500).send(e_2);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=user.routes.js.map