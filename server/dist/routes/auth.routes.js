"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.authRouter = void 0;
var bcrypt_1 = __importDefault(require("bcrypt"));
var crypto_1 = __importDefault(require("crypto"));
var express_1 = require("express");
var gender_enum_1 = require("../enums/gender.enum");
var account_model_1 = require("../models/account.model");
var user_info_model_1 = require("../models/user-info.model");
exports.authRouter = (0, express_1.Router)();
exports.authRouter.post('/register/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, login, password, name_1, surname, gender, dateOfBirth, phoneNumber, country, city, languages, existUser, newUserInfo, encryptedPassword, token, newAccount, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 5, , 6]);
                _a = req.body, login = _a.login, password = _a.password, name_1 = _a.name, surname = _a.surname, gender = _a.gender, dateOfBirth = _a.dateOfBirth, phoneNumber = _a.phoneNumber, country = _a.country, city = _a.city, languages = _a.languages;
                if (!((login === null || login === void 0 ? void 0 : login.trim()) && (password === null || password === void 0 ? void 0 : password.trim()))) {
                    res.status(400).send('Login and password are required');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login })];
            case 1:
                existUser = _b.sent();
                if (existUser) {
                    res.status(409).send('User Already Exist');
                    return [2 /*return*/];
                }
                newUserInfo = new user_info_model_1.UserInfoModel({
                    name: (name_1 === null || name_1 === void 0 ? void 0 : name_1.trim()) || undefined,
                    surname: (surname === null || surname === void 0 ? void 0 : surname.trim()) || undefined,
                    gender: gender || gender_enum_1.GenderEnum.UNMARKED,
                    dateOfBirth: dateOfBirth,
                    phoneNumber: phoneNumber,
                    country: (country === null || country === void 0 ? void 0 : country.trim()) || undefined,
                    city: (city === null || city === void 0 ? void 0 : city.trim()) || undefined,
                    languages: languages
                });
                return [4 /*yield*/, newUserInfo.save()];
            case 2:
                _b.sent();
                return [4 /*yield*/, bcrypt_1["default"].hash(password, 10)];
            case 3:
                encryptedPassword = _b.sent();
                token = crypto_1["default"].randomBytes(64).toString('hex');
                newAccount = new account_model_1.AccountModel({
                    login: login,
                    password: encryptedPassword,
                    sessionToken: token,
                    userInfo: newUserInfo._id
                });
                return [4 /*yield*/, newAccount.save()];
            case 4:
                _b.sent();
                res.status(200).send({
                    login: newAccount.login,
                    sessionToken: newAccount.sessionToken
                });
                return [3 /*break*/, 6];
            case 5:
                e_1 = _b.sent();
                res.status(500).send(e_1);
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); });
exports.authRouter.post('/login/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, login, password, account, token, e_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 6, , 7]);
                _a = req.body, login = _a.login, password = _a.password;
                if (!((login === null || login === void 0 ? void 0 : login.trim()) && (password === null || password === void 0 ? void 0 : password.trim()))) {
                    res.status(400).send('Login and password are required');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login })];
            case 1:
                account = _b.sent();
                if (!account) {
                    res.status(403).send('User does not exist');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, bcrypt_1["default"].compare(password, account.password)];
            case 2:
                if (!_b.sent()) return [3 /*break*/, 4];
                token = crypto_1["default"].randomBytes(64).toString('hex');
                account.sessionToken = token;
                return [4 /*yield*/, account.save()];
            case 3:
                _b.sent();
                res.status(200).send({
                    login: account.login,
                    sessionToken: account.sessionToken
                });
                return [3 /*break*/, 5];
            case 4:
                res.status(403).send('Wrong login or password');
                _b.label = 5;
            case 5: return [3 /*break*/, 7];
            case 6:
                e_2 = _b.sent();
                res.status(500).send(e_2);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
exports.authRouter.get('/logout/:login/', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, account, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _a.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                account.sessionToken = '';
                return [4 /*yield*/, account.save()];
            case 2:
                _a.sent();
                res.status(200).send();
                return [3 /*break*/, 4];
            case 3:
                e_3 = _a.sent();
                res.status(500).send(e_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=auth.routes.js.map