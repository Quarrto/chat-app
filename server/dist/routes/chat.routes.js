"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.chatRouter = void 0;
var express_1 = require("express");
var account_model_1 = require("../models/account.model");
var inbox_value_enum_1 = require("../enums/inbox-value.enum");
var message_model_1 = require("../models/message.model");
var chat_model_1 = require("../models/chat.model");
var user_info_model_1 = require("../models/user-info.model");
var index_1 = require("../index");
exports.chatRouter = (0, express_1.Router)();
// friendRouter.get('/getChatsByFields/:login', async (req, res) => {
//     try {
//         const login = req.params.login;
//         const token = req.headers.authorization.slice(7);
//         const fields = req.query.fields ? req.query.fields.toString().split(',').map(value => +value) :
//             [InboxValueEnum.ALL, InboxValueEnum.FRIENDS, InboxValueEnum.IMPORTANT, InboxValueEnum.UNREAD];
//
//         const account = await AccountModel.findOne({login, sessionToken: token});
//
//         if (!account) {
//             res.status(401).send('Unauthorized');
//             return;
//         }
//
//         const result = {};
//
//         for (const value of fields) {
//             const previe
//             result[value] = await getChatIdsByField(account, value);
//         }
//
//         res.status(200).send(result));
//     } catch (e) {
//         res.status(500).send(e);
//     }
// });
function getChatIdsByField(account, value) {
    return __awaiter(this, void 0, void 0, function () {
        var chats, _a, chatsIds, unreadMessages, unreadMessagesIds;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _a = value;
                    switch (_a) {
                        case inbox_value_enum_1.InboxValueEnum.ALL: return [3 /*break*/, 1];
                        case inbox_value_enum_1.InboxValueEnum.FRIENDS: return [3 /*break*/, 2];
                        case inbox_value_enum_1.InboxValueEnum.IMPORTANT: return [3 /*break*/, 4];
                        case inbox_value_enum_1.InboxValueEnum.UNREAD: return [3 /*break*/, 6];
                    }
                    return [3 /*break*/, 9];
                case 1: return [2 /*return*/, account.chats];
                case 2: return [4 /*yield*/, chat_model_1.ChatModel.find({ $and: [{ participants: { $in: account._id } }, { participants: { $in: account.friends } }] })];
                case 3:
                    // chats = await ChatModel.find({participants: {$in: account.friends}});
                    chats = _b.sent();
                    return [2 /*return*/, chats.map(function (value1) { return value1._id; })];
                case 4: return [4 /*yield*/, chat_model_1.ChatModel.find({ $and: [{ participants: { $in: account._id } }, { participants: { $in: account.important } }] })];
                case 5:
                    // chats = await ChatModel.find({participants: {$in: account.important}});
                    chats = _b.sent();
                    return [2 /*return*/, chats.map(function (value1) { return value1._id; })];
                case 6:
                    chatsIds = account.chats;
                    return [4 /*yield*/, message_model_1.MessageModel.find({
                            chatId: { $in: chatsIds },
                            read: false,
                            ownerId: { $ne: account._id }
                        })];
                case 7:
                    unreadMessages = _b.sent();
                    unreadMessagesIds = unreadMessages.map(function (value1) { return value1._id; });
                    return [4 /*yield*/, chat_model_1.ChatModel.find({ messages: { $in: unreadMessagesIds } })];
                case 8:
                    chats = _b.sent();
                    return [2 /*return*/, chats.map(function (value1) { return value1._id; })];
                case 9: return [2 /*return*/, []];
            }
        });
    });
}
exports.chatRouter.get('/getChatsPreviewByField/:login', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, token, field, account, chatsIds, result, _i, chatsIds_1, id, _a, _b, e_1;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 7, , 8]);
                login = req.params.login;
                token = req.headers.authorization.slice(7);
                field = +req.query.field || inbox_value_enum_1.InboxValueEnum.ALL;
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account = _c.sent();
                if (!account) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, getChatIdsByField(account, field)];
            case 2:
                chatsIds = _c.sent();
                result = [];
                _i = 0, chatsIds_1 = chatsIds;
                _c.label = 3;
            case 3:
                if (!(_i < chatsIds_1.length)) return [3 /*break*/, 6];
                id = chatsIds_1[_i];
                _b = (_a = result).push;
                return [4 /*yield*/, getChatPreview(account._id, id)];
            case 4:
                _b.apply(_a, [_c.sent()]);
                _c.label = 5;
            case 5:
                _i++;
                return [3 /*break*/, 3];
            case 6:
                res.status(200).send(result);
                return [3 /*break*/, 8];
            case 7:
                e_1 = _c.sent();
                res.status(500).send(e_1);
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
function getChatPreview(account, chatId) {
    var _a;
    return __awaiter(this, void 0, void 0, function () {
        var chat, lastMessage, chatMemberId, _b, login, userId, user, connectionLogins, unreadCount;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0: return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId)];
                case 1:
                    chat = _c.sent();
                    return [4 /*yield*/, message_model_1.MessageModel.find({ chatId: chatId }).sort({ 'date': -1 }).limit(1)];
                case 2:
                    lastMessage = (_c.sent())[0];
                    chatMemberId = chat.participants.find(function (value) { return value.toString() !== account._id.toString(); });
                    return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
                case 3:
                    _b = _c.sent(), login = _b.login, userId = _b.userInfo;
                    return [4 /*yield*/, user_info_model_1.UserInfoModel.findById(userId)];
                case 4:
                    user = _c.sent();
                    connectionLogins = Array.from(index_1.socketMap.values());
                    return [4 /*yield*/, message_model_1.MessageModel.count({ chatId: chatId, ownerId: chatMemberId, read: false })];
                case 5:
                    unreadCount = _c.sent();
                    return [2 /*return*/, {
                            chatId: chatId.toString(),
                            login: login,
                            name: user === null || user === void 0 ? void 0 : user.name,
                            surname: user === null || user === void 0 ? void 0 : user.surname,
                            isImportant: ((_a = account.important) === null || _a === void 0 ? void 0 : _a.includes(chatMemberId)) || false,
                            online: connectionLogins.includes(login),
                            unread: unreadCount,
                            message: !lastMessage ? undefined : {
                                message: lastMessage === null || lastMessage === void 0 ? void 0 : lastMessage.message,
                                owner: (lastMessage === null || lastMessage === void 0 ? void 0 : lastMessage.ownerId.toString()) === account._id.toString() ? account.login : login,
                                date: lastMessage === null || lastMessage === void 0 ? void 0 : lastMessage.date,
                                read: lastMessage === null || lastMessage === void 0 ? void 0 : lastMessage.read
                            }
                        }];
            }
        });
    });
}
exports.chatRouter.get('/getChat/:login/:chatId', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, chatId, token, limit, offset, account_1, chat, chatMemberId, _a, chatMemberLogin_1, userId, messagesCount, messages, user, connectionLogins, result, e_2;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 7, , 8]);
                login = req.params.login;
                chatId = req.params.chatId.toString();
                token = req.headers.authorization.slice(7);
                limit = +req.query.limit || 0;
                offset = +req.query.offset || 0;
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account_1 = _b.sent();
                if (!account_1) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId)];
            case 2:
                chat = _b.sent();
                if (!chat) {
                    res.status(404).send('Chat not found');
                    return [2 /*return*/];
                }
                if (!chat.participants.find(function (value) { return value.toString() === account_1._id.toString(); })) {
                    res.status(403).send('You do not have access to this chat');
                    return [2 /*return*/];
                }
                chatMemberId = chat.participants.find(function (value) { return value.toString() !== account_1._id.toString(); });
                return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
            case 3:
                _a = _b.sent(), chatMemberLogin_1 = _a.login, userId = _a.userInfo;
                return [4 /*yield*/, message_model_1.MessageModel.count({ chatId: chat._id })];
            case 4:
                messagesCount = _b.sent();
                return [4 /*yield*/, message_model_1.MessageModel.find({ chatId: chat._id }).sort({ date: -1 }).skip(offset).limit(limit)];
            case 5:
                messages = _b.sent();
                return [4 /*yield*/, user_info_model_1.UserInfoModel.findById(userId)];
            case 6:
                user = _b.sent();
                connectionLogins = Array.from(index_1.socketMap.values());
                result = {
                    chatId: chat._id.toString(),
                    login: chatMemberLogin_1,
                    name: user === null || user === void 0 ? void 0 : user.name,
                    surname: user === null || user === void 0 ? void 0 : user.surname,
                    isImportant: account_1.important.includes(chatMemberId),
                    online: connectionLogins.includes(chatMemberLogin_1),
                    count: messagesCount,
                    messages: messages.map(function (value) {
                        return {
                            message: value.message,
                            owner: value.ownerId.toString() === account_1._id.toString() ? account_1.login : chatMemberLogin_1,
                            date: value.date,
                            read: value.read
                        };
                    }).reverse()
                };
                res.status(200).send(result);
                return [3 /*break*/, 8];
            case 7:
                e_2 = _b.sent();
                res.status(500).send(e_2);
                return [3 /*break*/, 8];
            case 8: return [2 /*return*/];
        }
    });
}); });
exports.chatRouter.get('/getMessages/:login/:chatId', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var login, chatId, token, limit, offset, account_2, chat, chatMemberId, chatMemberLogin_2, messagesCount, messages, result, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                login = req.params.login;
                chatId = req.params.chatId.toString();
                token = req.headers.authorization.slice(7);
                limit = +req.query.limit || 0;
                offset = +req.query.offset || 0;
                return [4 /*yield*/, account_model_1.AccountModel.findOne({ login: login, sessionToken: token })];
            case 1:
                account_2 = _a.sent();
                if (!account_2) {
                    res.status(401).send('Unauthorized');
                    return [2 /*return*/];
                }
                return [4 /*yield*/, chat_model_1.ChatModel.findById(chatId)];
            case 2:
                chat = _a.sent();
                if (!chat) {
                    res.status(404).send('Chat not found');
                    return [2 /*return*/];
                }
                if (!chat.participants.find(function (value) { return value.toString() === account_2._id.toString(); })) {
                    res.status(403).send('You do not have access to this chat');
                    return [2 /*return*/];
                }
                chatMemberId = chat.participants.find(function (value) { return value.toString() !== account_2._id.toString(); });
                return [4 /*yield*/, account_model_1.AccountModel.findById(chatMemberId)];
            case 3:
                chatMemberLogin_2 = (_a.sent()).login;
                return [4 /*yield*/, message_model_1.MessageModel.count({ chatId: chat._id })];
            case 4:
                messagesCount = _a.sent();
                return [4 /*yield*/, message_model_1.MessageModel.find({ chatId: chat._id }).sort({ date: -1 }).skip(offset).limit(limit)];
            case 5:
                messages = _a.sent();
                result = {
                    chatId: chat._id.toString(),
                    count: messagesCount,
                    messages: messages.map(function (value) {
                        return {
                            message: value.message,
                            owner: value.ownerId.toString() === account_2._id.toString() ? account_2.login : chatMemberLogin_2,
                            date: value.date,
                            read: value.read
                        };
                    }).reverse()
                };
                res.status(200).send(result);
                return [3 /*break*/, 7];
            case 6:
                e_3 = _a.sent();
                res.status(500).send(e_3);
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=chat.routes.js.map