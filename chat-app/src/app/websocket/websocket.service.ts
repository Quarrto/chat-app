import { Inject, Injectable, OnDestroy } from '@angular/core';
import { interval, Observable, of, Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, first, map, takeWhile } from 'rxjs/operators';
import { WebSocketSubject, WebSocketSubjectConfig } from 'rxjs/webSocket';
import { IWsMessage, WebSocketConfig } from './websocket.interfaces';
import { config } from './websocket.config';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService implements OnDestroy {
  private wsMessages$?: Subject<IWsMessage<any>>;
  private webSocket$?: WebSocketSubject<IWsMessage<any>>;

  private connectionSub: Subscription;

  private connection$: Subject<boolean>;
  private connectionObs$: Observable<boolean>;
  private isConnected = false;

  private reconnectAttempts: number;
  private reconnectInterval: number;
  private config: WebSocketSubjectConfig<IWsMessage<any>>;

  private isReconnectable = true;
  private reconnection$?: Observable<number>;

  private onConnectFunctions: Function[] = [];

  constructor(@Inject(config) private wsConfig: WebSocketConfig) {
    this.connection$ = new Subject<boolean>();

    this.connectionObs$ = this.connection$.asObservable()
      .pipe(distinctUntilChanged());

    this.connectionSub = this.connectionObs$.subscribe((value => this.isConnected = value));

    this.reconnectInterval = wsConfig.reconnectInterval || 5000; // pause between connections
    this.reconnectAttempts = wsConfig.reconnectAttempts || 10; // number of connection attempts

    this.config = {
      url: wsConfig.url,
      closeObserver: {
        next: (event: CloseEvent) => {
          this.webSocket$ = undefined;

          if (this.isReconnectable && !this.reconnection$) {
            // console.log('config reconnect');
            // console.log([this.isReconnectable, this.reconnection$]);
            this.reconnect();
          }

          if (this.isConnected) {
            this.connection$.next(false);
          }
        }
      },
      openObserver: {
        next: (event: Event) => {
          // console.log('WebSocket connected!');
          this.connection$.next(true);
          this.reconnection$ = undefined;
          this.onConnectFunctions.forEach(value => value());
        }
      }
    };
  }

  ngOnDestroy() {
    this.connectionSub.unsubscribe();

    this.disconnect();
  }


  /*
  * connect to WebSocked
  * */
  public connect(): void {
    if (this.isConnected) {
      this.connection$
        .pipe(
          filter(value => !value),
          first())
        .subscribe(() => this.connect());

      return;
    }

    this.isReconnectable = true;

    this.wsMessages$ = new Subject<IWsMessage<any>>();

    this.wsMessages$.subscribe(
      null, (error: ErrorEvent) => console.error('WebSocket error!', error)
    );

    this.webSocket$ = new WebSocketSubject(this.config);

    this.webSocket$.subscribe(
      (message) => this.wsMessages$?.next(message),
      (error: Event) => {
        if (!this.webSocket$ && !this.isReconnectable) {
          // run reconnect if errors
          // console.log('connect reconnect');
          this.reconnect();
        }
      });
  }

  public disconnect(): Observable<boolean> {
    if (!this.isConnected) {
      return of(false);
    }

    this.isReconnectable = false;
    // this.isConnected = false;

    // console.log('disconnect');
    this.clearOnConnectFns();

    this.webSocket$?.complete();
    this.wsMessages$?.complete();

    return this.connectionObs$
      .pipe(filter(value => !value),
        first());
  }


  /*
  * reconnect if not connecting or errors
  * */
  private reconnect(): void {
    if (!this.isReconnectable) {
      return;
    }

    this.reconnection$ = interval(this.reconnectInterval)
      .pipe(takeWhile((value, index) => index < this.reconnectAttempts && !this.webSocket$));

    this.reconnection$.subscribe(
      () => {
        if (!this.isConnected && this.isReconnectable) {
          this.connect();
        }
      },
      null,
      () => {

        // Subject complete if reconnect attempts ending
        this.reconnection$ = undefined;

        if (!this.webSocket$) {
          this.disconnect();
        }
      });
  }


  /*
  * on message event
  * */
  public on<T>(event: string): Observable<T | undefined> {
    if (event && this.wsMessages$) {
      return this.wsMessages$.pipe(
        filter((message: IWsMessage<T>) => message.event === event),
        map((message: IWsMessage<T>) => message.data)
      );
    }

    // console.log(`error on event: ${event}`);
    return of(undefined);
  }


  /*
  * on message to server
  * */
  public send(event: string, data: any = {}): void {
    if (!this.isConnected) {
      this.connection$
        .pipe(
          filter(value => value),
          first())
        .subscribe(() => this.send(event, data));

      return;
    }

    if (event) {
      this.webSocket$?.next(<any>JSON.stringify({event, data}));
    } else {
      // console.log([event, this.isConnected]);
      console.error('Send error!');
    }
  }

  public addOnConnectFn(fn: Function) {
    this.onConnectFunctions.push(fn);
  }

  public clearOnConnectFns() {
    this.onConnectFunctions = [];
  }
}
