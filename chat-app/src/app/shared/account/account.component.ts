import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IChat } from '../../core/interfaces/chat-preview.interface';
import { IMessageEvent } from '../../core/interfaces/message-event.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { AuthService } from '../../core/services/auth.service';
import { ChatWsService } from '../../core/services/chat-ws.service';
import { EventService } from '../../core/services/event.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'chat-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.sass'],
  animations: [
    trigger(
      'appendPopUpAnimation',
      [
        transition(
          ':enter',
          [
            style({height: 0, opacity: 0}),
            animate('0.3s ease-out',
              style({height: 35, opacity: 1}))
          ]
        ),
        transition(
          ':leave',
          [
            style({height: 35, opacity: 1}),
            animate('0.3s ease-in',
              style({height: 0, opacity: 0}))
          ]
        )
      ]
    ),
    trigger(
      'appendNotificationsAnimation',
      [
        transition(
          ':enter',
          [
            style({height: 0, opacity: 0}),
            animate('0.3s ease-out',
              style({height: 200, opacity: 1}))
          ]
        ),
        transition(
          ':leave',
          [
            style({height: 200, opacity: 1}),
            animate('0.3s ease-in',
              style({height: 0, opacity: 0}))
          ]
        )
      ]
    )
  ]
})
export class AccountComponent implements OnInit, OnDestroy {
  @Input() user?: IUserStorage;
  public isActionsOpen = false;
  public isNotificationsOpen = false;
  public notifications: Array<IChat> = [];

  private readSub?: Subscription;

  constructor(private authService: AuthService,
              private storage: StorageService,
              private router: Router,
              private chatWs: ChatWsService,
              private events: EventService) {
  }

  ngOnInit(): void {
    this.chatWs.onMessage()
      .pipe(
        filter(value => value?.message?.owner !== this.user?.login)
      )
      .subscribe(value => this.notifications.unshift(value));

    this.readSub = this.events.on<IMessageEvent>('read')
      .pipe(
        filter(value => value.send)
      )
      .subscribe(value => this.notifications = this.notifications.filter(value1 => value1.chatId !== value.chatId));
  }

  ngOnDestroy(): void {
    this.readSub?.unsubscribe();
  }

  toggleActions() {
    this.isActionsOpen = !this.isActionsOpen;
  }

  toggleNotifications() {
    this.isNotificationsOpen = !this.isNotificationsOpen;
  }

  logOut() {
    if (!this.user) {
      return;
    }

    this.authService.logOut(this.user)
      .subscribe(() => {
        this.storage.deleteUser(this.user?.login as string);
        this.router.navigate(['']);
      });
  }
}
