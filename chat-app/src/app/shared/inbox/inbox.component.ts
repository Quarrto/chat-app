import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { FriendsService } from '../../core/services/friends.service';
import { InboxValueEnum } from '../../core/enums/inbox-value.enum';
import { EventService } from '../../core/services/event.service';
import { merge, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { IMessageEvent } from '../../core/interfaces/message-event.interface';

@Component({
  selector: 'chat-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.sass']
})
export class InboxComponent implements OnInit, OnChanges, OnDestroy {
  @Input() user?: IUserStorage;
  @Input() selectedOption: InboxValueEnum = InboxValueEnum.ALL;
  @Output() toggleButtonCLick = new EventEmitter();
  @Output() selectedOptionChange = new EventEmitter<InboxValueEnum>();

  public inboxOptions = [
    {
      index: InboxValueEnum.ALL,
      name: 'All messages',
      count: 0,
      selected: true
    },
    {
      index: InboxValueEnum.FRIENDS,
      name: 'Friends',
      count: 0,
      selected: false
    },
    {
      index: InboxValueEnum.UNREAD,
      name: 'Unread',
      count: 0,
      selected: false
    },
    {
      index: InboxValueEnum.IMPORTANT,
      name: 'Important',
      count: 0,
      selected: false
    },
  ];
  public isLoaded = false;

  private updateSub?: Subscription;

  constructor(private friendsService: FriendsService, private events: EventService) {
  }

  ngOnInit(): void {
    this.updateSub = merge(
      this.events.on<IMessageEvent>('read'),
      this.events.on('important'),
      this.events.on('friends'),
      this.events.on<IMessageEvent>('message')
        .pipe(filter(value => !value.send))
    )
      .subscribe(() => {
        this.updateValues();
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.user) {
      this.isLoaded = false;
      this.updateValues();
    }
  }

  ngOnDestroy(): void {
    this.updateSub?.unsubscribe();
  }

  updateValues() {
    if (!this.user) {
      return;
    }

    this.friendsService.getValues(this.user)
      .subscribe(newValues => {
        this.isLoaded = true;
        this.inboxOptions.forEach((value => {
          value.count = newValues[value.index];
        }));
      });
  }

  onButtonClick() {
    this.toggleButtonCLick.emit();
  }

  changeOption(index: InboxValueEnum) {
    this.inboxOptions.forEach((value => {
      value.selected = value.index === index;
    }));
    this.selectedOption = index;

    this.selectedOptionChange.emit(this.selectedOption);
  }
}
