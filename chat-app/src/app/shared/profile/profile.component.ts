import { Component, Input, OnInit } from '@angular/core';
import { IUserInfo } from '../../core/interfaces/user-info.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { EventService } from '../../core/services/event.service';
import { UserService } from '../../core/services/user.service';

@Component({
  selector: 'chat-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  @Input() user?: IUserStorage;

  public profile?: IUserInfo;
  public profileLogin?: string;

  constructor(private userService: UserService,
              private events: EventService
  ) {
  }

  ngOnInit(): void {
    this.events.on<string | undefined>('profile')
      .subscribe((value => {
        this.profileLogin = value;
        if (!this.user || !this.profileLogin) {
          this.profile = undefined;
          return;
        }

        this.userService.getUserInfo(this.profileLogin, this.user)
          .subscribe((value1 => this.profile = value1));
      }));
  }
}
