import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'chat-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required]
  });


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private storageService: StorageService,
    private notifications: NotificationsService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.storageService.getUser(this.loginForm?.get('login')?.value)) {
      this.notifications.error('You are already logged in');
      return;
    }

    this.authService.login(this.loginForm?.get('login')?.value, this.loginForm?.get('password')?.value)
      .subscribe((entity) => {
        try {
          this.storageService.addNewUser(entity.login, entity.sessionToken);
          this.router.navigate([`id/${entity.login}`]);
        } catch (e) {
          this.notifications.error('Login error', e.message);
        }
      });
  }
}
