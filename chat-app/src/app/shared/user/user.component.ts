import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { distinctUntilChanged } from 'rxjs/operators';
import { InboxValueEnum } from '../../core/enums/inbox-value.enum';
import { IAccount } from '../../core/interfaces/account.interface';
import { IMessageEvent } from '../../core/interfaces/message-event.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { ChatWsService } from '../../core/services/chat-ws.service';
import { EventService } from '../../core/services/event.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'chat-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit, OnDestroy {
  public login = '';
  public user?: IUserStorage;
  public isInbox = true;
  public inboxOption: InboxValueEnum = InboxValueEnum.ALL;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private storageService: StorageService,
              private chatWs: ChatWsService,
              private notifications: NotificationsService,
              private events: EventService) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(distinctUntilChanged())
      .subscribe(value => {
        this.login = value.login;
        this.user = this.storageService.getUser(this.login);

        if (!this.user) {
          this.router.navigate(['']);
          return;
        }

        this.chatWs.disconnect()
          .subscribe(() => this.chatWs.connect(this.user as IUserStorage));
      });

    this.chatWs.onError()
      .subscribe((err) => {
        this.notifications.error(`WS error ${err.code}`, err.message);
      });

    this.events.clearAll();
    this.events.create('important');
    this.events.create('read');
    this.events.create('message');
    this.events.create('friends');

    this.chatWs.onRead()
      .subscribe((value) => this.events.send<IMessageEvent>('read', {chatId: value.chatId, send: false}));
    //
    this.chatWs.onMessage()
      .subscribe((value) => this.events.send<IMessageEvent>('message', {chatId: value.chatId, send: false}));

    this.chatWs.onFriends()
      .subscribe(value => this.events.send<IAccount>('friends', value));
  }

  ngOnDestroy(): void {
    this.chatWs.disconnect();
  }

  toggleInbox() {
    this.isInbox = !this.isInbox;
  }
}
