import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenderEnum } from '../../core/enums/gender.enum';
import { AuthService } from '../../core/services/auth.service';
import { IAccount } from '../../core/interfaces/account.interface';
import * as _ from 'lodash';
import { IUserInfo } from '../../core/interfaces/user-info.interface';
import { Router } from '@angular/router';
import { StorageService } from '../../core/services/storage.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'chat-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup = this.fb.group({
    account: this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      confirm: ['', Validators.required]
    }),
    userInfo: this.fb.group({
      name: [''],
      surname: [''],
      country: [''],
      city: [''],
      dob: [''],
      phone: [''],
      gender: [GenderEnum.UNMARKED],
      language: ['']
    })
  });

  public languages: Array<string> = [];
  public genderEnum = GenderEnum;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private storageService: StorageService,
    private notifications: NotificationsService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const accountForm = this.registerForm.get('account');
    const userInfoForm = this.registerForm.get('userInfo');

    if (accountForm?.get('password')?.value !== accountForm?.get('confirm')?.value) {
      this.notifications.error('Passwords are not equal');
      return;
    }

    const account: IAccount = {
      login: accountForm?.get('login')?.value,
      password: accountForm?.get('password')?.value,
    };

    let userInfo: IUserInfo = {
      name: userInfoForm?.get('name')?.value?.trim(),
      surname: userInfoForm?.get('surname')?.value?.trim(),
      country: userInfoForm?.get('country')?.value?.trim(),
      city: userInfoForm?.get('city')?.value?.trim(),
      dateOfBirth: !!userInfoForm?.get('dob')?.value ? +new Date(userInfoForm?.get('dob')?.value) : undefined,
      phoneNumber: userInfoForm?.get('phone')?.value?.trim(),
      gender: userInfoForm?.get('gender')?.value,
      languages: this.languages
    };

    userInfo = _.omitBy(userInfo, (value) => {
      return value === '' || _.isNil(value);
    });

    this.authService.register(account, userInfo)
      .subscribe((entity) => {
        try {
          this.storageService.addNewUser(entity.login, entity.sessionToken);
          this.router.navigate([`id/${entity.login}`]);
        } catch (e) {
          this.notifications.error('Login error', e.message);
        }
      });
  }

  addLanguage(value: string) {
    value = value.trim();
    if (value && !this.languages.includes(value)) {
      this.languages.push(value);
      this.registerForm?.get('userInfo')?.get('language')?.setValue('');
    }
  }

  deleteLanguage(index: number) {
    this.languages.splice(index, 1);
  }
}
