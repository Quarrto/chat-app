import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { merge, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { InboxValueEnum } from '../../core/enums/inbox-value.enum';
import { IChat } from '../../core/interfaces/chat-preview.interface';
import { IMessageEvent } from '../../core/interfaces/message-event.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { ChatWsService } from '../../core/services/chat-ws.service';
import { ChatService } from '../../core/services/chat.service';
import { EventService } from '../../core/services/event.service';
import { ResizeRxjsService } from '../../core/services/resize-rxjs.service';

@Component({
  selector: 'chat-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.sass']
})
export class ContactsComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  constructor(
    private chatService: ChatService,
    private chatWs: ChatWsService,
    private events: EventService,
    private resizeService: ResizeRxjsService,
    private ref: ChangeDetectorRef
  ) {
  }
  @Input() inboxOption: InboxValueEnum = InboxValueEnum.ALL;
  @Input() user?: IUserStorage;
  @ViewChild('contacts', {static: false}) contactsElem?: ElementRef;
  public chats: IChat[] = [];

  public querySearch: FormControl = new FormControl('');
  public isLoaded = false;
  public letters = 35;

  private onlineSub?: Subscription;
  private messageSub?: Subscription;
  private importantSub?: Subscription;
  private friendsSub?: Subscription;
  private resizeObs$?: Observable<ResizeObserverEntry>;

  // 390 - 35
  // 310 - 21
  static getMessageSize(width: number) {
    return Math.round(14 * (width - 310) / 80 + 21);
  }

  ngOnInit(): void {
    this.onlineSub = this.chatWs.onOnline().subscribe((value => {
      const chatStatus = this.chats.find((value1 => value1.login === value.login));

      if (chatStatus) {
        chatStatus.online = value.online;
      }
    }));

    this.messageSub = merge(
      this.events.on<IMessageEvent>('read'),
      this.events.on<IMessageEvent>('message')
        .pipe(filter(value => !value.send))
    )
      .subscribe(() => {
        this.updatePreviews();
      });

    this.importantSub = this.events.on('important')
      .pipe(
        filter(() => this.inboxOption === InboxValueEnum.IMPORTANT)
      )
      .subscribe(() => this.updatePreviews());

    this.friendsSub = this.events.on('friends')
      .pipe(
        filter(() => this.inboxOption === InboxValueEnum.FRIENDS || this.inboxOption === InboxValueEnum.ALL)
      )
      .subscribe(() => this.updatePreviews());
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isLoaded = false;
    this.updatePreviews();
  }

  ngAfterViewInit(): void {
    if (!this.contactsElem) {
      return;
    }

    this.resizeObs$ = this.resizeService.createObseravble(this.contactsElem.nativeElement);

    this.resizeObs$
      .pipe(
        map(value => value.contentRect.width),
        distinctUntilChanged(),
        map(ContactsComponent.getMessageSize),
        distinctUntilChanged()
      )
      .subscribe(value => {
        // console.log(value);
        this.letters = value;
        this.ref.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.onlineSub?.unsubscribe();
    this.messageSub?.unsubscribe();
    this.importantSub?.unsubscribe();
    this.friendsSub?.unsubscribe();

    if (this.contactsElem) {
      this.resizeService.unsubscribe(this.contactsElem.nativeElement);
    }
  }

  getChatName(chat: IChat) {
    return chat.name || chat.surname ? `${chat.name ? chat.name + ' ' : ''}${chat.surname || ''}` : chat.login;
  }

  getSearchChats() {
    return this.chats.filter((value => {
      const query = this.querySearch.value;
      return value.login?.match(query) || value.name?.match(query) || value.surname?.match(query);
    }));
  }

  updatePreviews() {
    if (this.user) {
      this.chatService.getPreviewsByField(this.inboxOption, this.user)
        .subscribe(value => {
          this.isLoaded = true;
          this.chats = value;
        });
    }
  }

}
