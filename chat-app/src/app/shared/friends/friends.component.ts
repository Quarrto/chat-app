import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { IAccount } from '../../core/interfaces/account.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { EventService } from '../../core/services/event.service';
import { FriendsService } from '../../core/services/friends.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'chat-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.sass']
})
export class FriendsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() user?: IUserStorage;
  @Output() toggleButtonCLick = new EventEmitter();

  public isOpenReceivedInvites = false;
  public isOpenPendingInvites = false;
  public receivedInvites: Array<string> = [];
  public pendingInvites: Array<string> = [];
  public friends: Array<string> = [];
  public searchResults: Array<string> = [];

  public searchForm: FormGroup = this.fb.group({
    query: ['']
  });

  private searchSub?: Subscription;
  private friendsSub?: Subscription;

  constructor(
    private friendsService: FriendsService,
    private storageService: StorageService,
    private fb: FormBuilder,
    private events: EventService
  ) {
  }

  ngOnInit(): void {
    this.searchSub = this.searchForm.get('query')?.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((query) => this.friendsService.search(query, this.user as IUserStorage))
      )
      .subscribe((value => {
        this.searchResults = value;
      }));

    this.friendsSub = this.events.on<IAccount>('friends')
      .subscribe((value) => {
        this.updateFriends(value);
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.friendsService.getInvites(changes.user.currentValue)
      .subscribe(value => {
        this.receivedInvites = value.sentFriends || [];
        this.pendingInvites = value.pendingFriends || [];
        this.friends = value.friends || [];
      });
  }

  ngOnDestroy(): void {
    this.searchSub?.unsubscribe();
    this.friendsSub?.unsubscribe();
  }

  onButtonClick() {
    this.toggleButtonCLick.emit();
  }

  toggleReceivedList() {
    this.isOpenReceivedInvites = !this.isOpenReceivedInvites;
  }

  togglePendingList() {
    this.isOpenPendingInvites = !this.isOpenPendingInvites;
  }

  search(query: string) {
    this.friendsService.search(query, this.user as IUserStorage).subscribe((value => {
      this.searchResults = value;
    }));
  }

  addFriend(login: string) {
    this.friendsService.add(login, this.user as IUserStorage).subscribe((value => {
      this.events.send<IAccount>('friends', value);
    }));
  }

  declineFriend(login: string) {
    this.friendsService.decline(login, this.user as IUserStorage).subscribe((value => {
      this.events.send<IAccount>('friends', value);
    }));
  }

  isAddButtonVisible(login: string) {
    return !(this.friends.includes(login) || this.pendingInvites.includes(login) || this.user?.login === login);
  }

  updateFriends(newValue: IAccount) {
    this.receivedInvites = newValue.sentFriends || [];
    this.pendingInvites = newValue.pendingFriends || [];
    this.friends = newValue.friends || [];
  }
}
