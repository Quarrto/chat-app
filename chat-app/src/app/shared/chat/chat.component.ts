import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { fromEvent, NEVER, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import { IChat } from '../../core/interfaces/chat-preview.interface';
import { IMessage } from '../../core/interfaces/message.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { ChatWsService } from '../../core/services/chat-ws.service';
import { ChatService } from '../../core/services/chat.service';
import { EventService } from '../../core/services/event.service';
import { FriendsService } from '../../core/services/friends.service';

@Component({
  selector: 'chat-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.sass']
})
export class ChatComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @Input() chat?: IChat;
  @Input() user?: IUserStorage;
  @ViewChild('chatArea', {static: false}) chatArea?: ElementRef;

  public isTyping = false;
  public isLoaded = false;

  public messageForm: FormGroup = this.fb.group({
    message: ['', Validators.required]
  });

  private typingSub?: Subscription;
  private messageSub?: Subscription;
  private readSub?: Subscription;
  private scrollSub?: Subscription;

  private isReadyUpdate = true;
  private scrollPos = 0;

  constructor(private friendsService: FriendsService,
              private chatService: ChatService,
              private chatWs: ChatWsService,
              private fb: FormBuilder,
              private events: EventService) {
  }

  ngOnInit(): void {
    this.typingSub = this.chatWs.onTyping().subscribe(value => {
      if (value.chatId === this.chat?.chatId) {
        this.isTyping = value.typing;
      }
    });

    this.messageSub = this.chatWs.onMessage().subscribe(value => {
      if (this.chat && this.chat.chatId === value.chatId) {
        this.chat.messages?.push(value.message as IMessage);

        if (this.user && value?.message?.owner !== this.user.login) {
          this.chatWs.sendRead(this.user, this.chat.chatId);
          // this.events.send('read');
        }
      }
    });

    this.readSub = this.chatWs.onRead().subscribe(value => {
      if (this.chat && this.chat.chatId === value.chatId) {
        this.chat.messages?.forEach(message => {
          if (!message.read && message.owner === this.user?.login) {
            message.read = true;
          }
        });
      }

      // this.events.send('read');
    });

    this.messageForm.get('message')?.statusChanges
      .pipe(distinctUntilChanged())
      .subscribe(value => {
        if (!(this.user && this.chat)) {
          return;
        }

        if (value === 'VALID') {
          this.chatWs.sendTyping(this.user, this.chat.chatId, true);
        } else if (value === 'INVALID') {
          this.chatWs.sendTyping(this.user, this.chat.chatId, false);
        }
      });

    this.events.on('important')
      .subscribe(() => {
        if (!this.chat) {
          return;
        }

        this.chat.isImportant = !this.chat?.isImportant;
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.chat) {
      this.isLoaded = false;
    }

    if (changes.user) {
      if (_.isEqual(changes.user.previousValue, changes.user.currentValue)) {
        return;
      }
    }

    if (changes.chat) {
      if (_.isEqual(changes.chat.previousValue, changes.chat.currentValue)) {
        return;
      }
    }

    if (this.chat && this.user) {
      this.isLoaded = true;
      this.chatWs.sendRead(this.user, this.chat.chatId);
    }
  }

  ngAfterViewInit(): void {
    this.scrollSub = fromEvent<Event>(this.chatArea?.nativeElement, 'scroll')
      .pipe(
        debounceTime(300),
        map((value: Event) => {
          return (value.target as Element).scrollHeight + (value.target as Element).scrollTop <=
            (value.target as Element).clientHeight + 5;
        }),
        // distinctUntilChanged(),
        filter(value => value),
        filter(() => this.isLoadMore()),
        filter(() => this.isReadyUpdate),
        tap(() => this.scrollPos = (this.chatArea?.nativeElement as Element).scrollTop),
        switchMap(() => {
          this.isReadyUpdate = false;
          if (!this.chat || !this.user) {
            return NEVER;
          }
          return this.chatService.getMessages(this.chat?.chatId, this.user, this.chat?.messages?.length as number);
        })
      )
      .subscribe(value => {
        if (!value || !this.chat) {
          this.isReadyUpdate = true;
          return;
        }

        const chatEl = this.chatArea?.nativeElement as Element;
        chatEl.scrollTo(0, this.scrollPos + 50);
        this.chat.count = value.count;
        this.chat.messages?.unshift(...(value?.messages?.sort((a, b) => +a.date - +b.date) || []));
        this.isReadyUpdate = true;
      });
  }

  ngOnDestroy(): void {
    this.messageSub?.unsubscribe();
    this.readSub?.unsubscribe();
    this.typingSub?.unsubscribe();
    this.scrollSub?.unsubscribe();
  }

  getName() {
    return this.chat?.name || this.chat?.surname ?
      `${this.chat?.name ? this.chat?.name + ' ' : ''}${this.chat?.surname || ''}` :
      this.chat?.login;
  }

  toggleImportant() {
    if (this.chat?.isImportant) {
      this.friendsService.deleteImportant(this.chat?.login as string, this.user as IUserStorage)
        .subscribe((() => {
          if (this.chat) {
            // this.chat.isImportant = value.includes(this.chat?.login as string);
            this.events.send('important');
          }
        }));

      return;
    }

    this.friendsService.addImportant(this.chat?.login as string, this.user as IUserStorage)
      .subscribe((() => {
        if (this.chat) {
          // this.chat.isImportant = value.includes(this.chat?.login as string);
          this.events.send('important');
        }
      }));
  }

  sendMessage() {
    if (!(this.user && this.chat)) {
      return;
    }

    this.chatWs.sendMessage(this.user, this.chat?.chatId, this.messageForm.get('message')?.value);

    this.messageForm.reset();
    (this.chatArea?.nativeElement as Element).scrollTo(0, 0);
  }

  isLoadMore(): boolean {
    return !!this.chat?.messages && !!this.chat?.count && this.chat?.messages?.length < this.chat?.count;
  }
}
