import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { IAccount } from '../../core/interfaces/account.interface';
import { IUserInfo } from '../../core/interfaces/user-info.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { EventService } from '../../core/services/event.service';
import { FriendsService } from '../../core/services/friends.service';

@Component({
  selector: 'chat-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.sass'],
  animations: [
    trigger(
      'appendPopUpAnimation',
      [
        transition(
          ':enter',
          [
            style({height: 0, opacity: 0}),
            animate('0.3s ease-out',
              style({height: 70, opacity: 1}))
          ]
        ),
        transition(
          ':leave',
          [
            style({height: 70, opacity: 1}),
            animate('0.3s ease-in',
              style({height: 0, opacity: 0}))
          ]
        )
      ]
    )
  ]
})
export class ProfileInfoComponent implements OnInit, OnDestroy, OnChanges {
  @Input() profile?: IUserInfo;
  @Input() profileLogin?: string;
  @Input() user?: IUserStorage;

  public isActionsOpen = false;
  public isLoaded = false;

  private importantSub?: Subscription;
  private friendsSub?: Subscription;

  constructor(private events: EventService,
              private friendsService: FriendsService) {
  }

  ngOnInit(): void {
    this.importantSub = this.events.on('important')
      .subscribe(() => {
        if (!this.profile) {
          return;
        }

        this.profile.isImportant = !this.profile?.isImportant;
      });

    this.friendsSub = this.events.on<IAccount>('friends')
      .subscribe((value) => {
        this.updateFriends(value);
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.profile) {
      this.isLoaded = true;
    } else {
      this.isLoaded = false;
    }
  }

  ngOnDestroy(): void {
    this.importantSub?.unsubscribe();
    this.friendsSub?.unsubscribe();
  }

  getName() {
    return this.profile?.name || this.profile?.surname ?
      `${this.profile?.name ? this.profile?.name + ' ' : ''}${this.profile?.surname || ''}` :
      this.profileLogin;
  }

  getLocation() {
    return this.profile?.city || this.profile?.country ?
      `${this.profile?.city ? this.profile?.city + ' ' : ''}${this.profile?.country || ''}` : '';
  }

  isLanguagesIndicated() {
    return !_.isEmpty(this.profile?.languages);
  }

  toggleImportant() {
    if (this.profile?.isImportant) {
      this.friendsService.deleteImportant(this.profileLogin as string, this.user as IUserStorage)
        .subscribe((() => {
          if (this.profile) {
            this.events.send('important');
          }
        }));

      return;
    }

    this.friendsService.addImportant(this.profileLogin as string, this.user as IUserStorage)
      .subscribe((() => {
        if (this.profile) {
          this.events.send('important');
        }
      }));
  }

  addFriend() {
    this.friendsService.add(this.profileLogin as string, this.user as IUserStorage).subscribe((value => {
      // this.updateFriends(value);
      this.events.send<IAccount>('friends', value);
    }));
  }

  declineFriend() {
    this.friendsService.decline(this.profileLogin as string, this.user as IUserStorage).subscribe((value => {
      // this.updateFriends(value);
      this.events.send<IAccount>('friends', value);
    }));
  }

  deleteFriend() {
    this.friendsService.delete(this.profileLogin as string, this.user as IUserStorage).subscribe((value => {
      // this.updateFriends(value);
      this.events.send<IAccount>('friends', value);
    }));
  }

  updateFriends(newValue: IAccount) {
    if (!this.profile) {
      return;
    }

    this.profile.isSentFriend = newValue.sentFriends?.includes(this.profileLogin as string);
    this.profile.isPendingFriend = newValue.pendingFriends?.includes(this.profileLogin as string);
    this.profile.isFriend = newValue.friends?.includes(this.profileLogin as string);
  }

  toggleActions() {
    this.isActionsOpen = !this.isActionsOpen;
  }
}
