import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IChat } from '../../core/interfaces/chat-preview.interface';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';
import { ChatService } from '../../core/services/chat.service';
import { EventService } from '../../core/services/event.service';
import { StorageService } from '../../core/services/storage.service';

@Component({
  selector: 'chat-chat-area',
  templateUrl: './chat-area.component.html',
  styleUrls: ['./chat-area.component.sass']
})
export class ChatAreaComponent implements OnInit, OnDestroy {
  public user?: IUserStorage;
  public chat?: IChat;

  private chatSub?: Subscription;
  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private chatService: ChatService,
    private events: EventService
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(value => {
      this.user = this.storageService.getUser(value.login);

      if (value.chatId && this.user) {
        // console.log('if');
        this.chat = undefined;
        this.events.send<string | undefined>('profile', undefined);

        this.chatService.getChat(value.chatId, this.user as IUserStorage)
          .subscribe(value1 => {
            this.chat = value1;
            this.events.send<string | undefined>('profile', this.chat?.login);
          });
      }
    });
  }

  ngOnDestroy(): void {
    this.events.send<string | undefined>('profile', undefined);
    this.chatSub?.unsubscribe();
  }
}
