import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';
import { IUserStorage } from '../../core/interfaces/user-storage.interface';

@Component({
  selector: 'chat-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  public users: Array<IUserStorage> = [];

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
    this.users = this.storageService.getAllUsers();

    this.storageService.storageChanged.subscribe(() => {
      this.users = this.storageService.getAllUsers();
    });
  }

}
