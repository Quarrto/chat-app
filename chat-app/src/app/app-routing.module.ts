import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { UserComponent } from './shared/user/user.component';
import { RegisterComponent } from './shared/register/register.component';
import { ChatAreaComponent } from './shared/chat-area/chat-area.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: 'id/:login', component: UserComponent, children: [
      {path: 'chat/:chatId', component: ChatAreaComponent}
    ]
  }
];

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always'
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routingConfiguration)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
