import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextSlicePipe } from './core/pipes/text-slice.pipe';
import { DashboardComponent } from './shared/dashboard/dashboard.component';
import { LoginComponent } from './shared/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { UserComponent } from './shared/user/user.component';
import { RegisterComponent } from './shared/register/register.component';
import { InboxComponent } from './shared/inbox/inbox.component';
import { ContactsComponent } from './shared/contacts/contacts.component';
import { ChatComponent } from './shared/chat/chat.component';
import { ProfileComponent } from './shared/profile/profile.component';
import { FriendsComponent } from './shared/friends/friends.component';
import { ChatAreaComponent } from './shared/chat-area/chat-area.component';
import { GenderPipe } from './core/pipes/gender.pipe';
import { WebsocketModule } from './websocket/websocket.module';
import { environment } from '../environments/environment';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CachingInterceptor } from './http-interceptors/catching-interceptor';
import { MessageDatePipe } from './core/pipes/message-date.pipe';
import { LoadingComponent } from './shared/loading/loading.component';
import { AccountComponent } from './shared/account/account.component';
import { ProfileInfoComponent } from './shared/profile-info/profile-info.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UserComponent,
    RegisterComponent,
    InboxComponent,
    ContactsComponent,
    ChatComponent,
    ProfileComponent,
    FriendsComponent,
    ChatAreaComponent,
    GenderPipe,
    MessageDatePipe,
    LoadingComponent,
    AccountComponent,
    ProfileInfoComponent,
    TextSlicePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    WebsocketModule.config({
      url: environment.wsURL
    }),
    SimpleNotificationsModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CachingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
