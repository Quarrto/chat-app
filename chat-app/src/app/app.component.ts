import { Component } from '@angular/core';
import { NotificationAnimationType, Options } from 'angular2-notifications';

@Component({
  selector: 'chat-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'chat-app';

  public options: Options = {
    position: ['bottom', 'right'],
    timeOut: 5000,
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: false,
    maxStack: 3,
    animate: NotificationAnimationType.Fade,
    preventDuplicates: true
  };
}
