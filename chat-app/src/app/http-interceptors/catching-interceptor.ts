import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NEVER, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import { StorageService } from '../core/services/storage.service';

@Injectable()
export class CachingInterceptor implements HttpInterceptor {
  constructor(public notifications: NotificationsService,
              private router: Router,
              private storage: StorageService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse): Observable<HttpEvent<any>> => {
        if (err.status === 500) {
          this.notifications.error('Internal server error', err.error);
          return NEVER;
        } else if (err.status === 401) {
          this.notifications.error('Unauthorized');
          const login = this.router.parseUrl(this.router.url)?.root?.children?.primary?.segments[1]?.path;

          if (login) {
            this.storage.deleteUser(login);
          }

          this.router.navigate(['']);
          return NEVER;
        } else if (err.status < 500 && err.status >= 400) {
          console.log(err);
          this.notifications.error(err.error);
          return NEVER;
        }
        this.notifications.error('Server is disabled');
        return NEVER;
      })
    );
  }
}
