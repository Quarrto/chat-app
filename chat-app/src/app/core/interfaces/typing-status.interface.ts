export interface ITypingStatus {
  chatId: string;
  typing: boolean;
}
