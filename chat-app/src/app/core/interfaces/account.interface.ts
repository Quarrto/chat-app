/**
 * @interface IAccount contains basic information about user
 */
export interface IAccount {
  login?: string;
  password?: string;
  friends?: string[];
  pendingFriends?: string[];
  sentFriends?: string[];
  important?: string[];
  sessionToken?: string;
}
