export interface IMessage {
  message: string;
  owner: string;
  date: number | Date;
  read: boolean;
}
