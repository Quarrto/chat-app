export interface IWsError {
  code: number;
  message: string;
}
