export interface IUserStorage {
  login: string;
  sessionToken: string;
}
