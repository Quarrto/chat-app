import { GenderEnum } from '../enums/gender.enum';

export interface IUserInfo {
  name?: string;
  surname?: string;
  gender?: GenderEnum;
  dateOfBirth?: Date | number;
  phoneNumber?: string;
  country?: string;
  city?: string;
  languages?: string[];
  isImportant?: boolean;
  isFriend?: boolean;
  isPendingFriend?: boolean;
  isSentFriend?: boolean;
}
