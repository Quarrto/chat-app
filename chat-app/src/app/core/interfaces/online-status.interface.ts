export interface IOnlineStatus {
  login: string;
  online: boolean;
}
