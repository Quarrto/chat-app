export interface IMessageEvent {
  chatId: string;
  send: boolean;
}
