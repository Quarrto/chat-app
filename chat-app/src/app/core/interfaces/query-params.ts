/**
 * Interface for query params
 */
export interface IQueryParams {
  [param: string]: string | number | boolean;
}
