import { IMessage } from './message.interface';

export interface IChat {
  chatId: string;
  login?: string;
  name?: string;
  surname?: string;
  isImportant?: boolean;
  online?: boolean;
  unread?: number;
  count?: number;
  message?: IMessage;
  messages?: IMessage[];
}
