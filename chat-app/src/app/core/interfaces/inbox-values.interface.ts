import { InboxValueEnum } from '../enums/inbox-value.enum';

export interface IInboxValues {
  [InboxValueEnum.ALL]: number;
  [InboxValueEnum.FRIENDS]: number;
  [InboxValueEnum.UNREAD]: number;
  [InboxValueEnum.IMPORTANT]: number;
}
