import { TestBed } from '@angular/core/testing';

import { ResizeRxjsService } from './resize-rxjs.service';

describe('ResizeRxjsService', () => {
  let service: ResizeRxjsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResizeRxjsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
