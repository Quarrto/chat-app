import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { IAccount } from '../interfaces/account.interface';
import { IUserInfo } from '../interfaces/user-info.interface';
import { IUserStorage } from '../interfaces/user-storage.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = environment.backendURL + 'auth/';

  constructor(private httpClient: HttpClient) {
  }

  public login(login: string, password: string): Observable<IUserStorage> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json']);

    return this.httpClient.post<IUserStorage>(`${this.url}login/`, {login, password}, {headers});
  }

  public register(account: IAccount, userInfo?: IUserInfo): Observable<IUserStorage> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json']);

    return this.httpClient.post<IUserStorage>(`${this.url}register/`, {...account, ...userInfo}, {headers});
  }

  public logOut(account: IUserStorage): Observable<void> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<void>(`${this.url}logout/${account.login}`, {headers});
  }
}
