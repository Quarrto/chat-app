import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private events = new Map<string, Subject<any>>();

  constructor() {
  }

  public on<T>(eventName: string): Observable<T> {
    let event = this.events.get(eventName);

    if (!event) {
      this.create<T>(eventName);
      event = this.events.get(eventName);
    }

    return event?.asObservable() as Observable<T>;
  }

  public send<T>(eventName: string, data?: T) {
    let event = this.events.get(eventName);

    if (!event) {
      this.create<T>(eventName);
      event = this.events.get(eventName);
    }

    event?.next(data);
  }

  public create<T>(eventName: string) {
    if (this.events.has(eventName)) {
      return;
    }

    this.events.set(eventName, new Subject<T>());
  }

  public clearAll() {
    this.events.forEach((event => {
      event.complete();
    }));

    this.events.clear();
  }

  public delete(eventName: string) {
    if (!this.events.has(eventName)) {
      return;
    }

    this.events.get(eventName)?.complete();
    this.events.delete(eventName);
  }
}
