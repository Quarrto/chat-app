import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResizeRxjsService {
  private resizeSubject: Subject<ResizeObserverEntry>;
  private observer?: ResizeObserver;

  constructor() {
    this.resizeSubject = new Subject<ResizeObserverEntry>();
  }

  public createObseravble(element: Element): Observable<ResizeObserverEntry> {
    this.observer = new ResizeObserver(([entry]) => this.resizeSubject.next(entry));
    this.observer.observe(element);

    return this.resizeSubject.asObservable();
  }

  public unsubscribe(element: Element) {
    this.observer?.unobserve(element);
    this.observer?.disconnect();
    this.resizeSubject.complete();
  }
}
