import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IUserStorage } from '../interfaces/user-storage.interface';
import { Observable } from 'rxjs';
import { InboxValueEnum } from '../enums/inbox-value.enum';
import { IChat } from '../interfaces/chat-preview.interface';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private url = environment.backendURL + 'chat/';

  constructor(private httpClient: HttpClient) {
  }

  public getPreviewsByField(field: InboxValueEnum, account: IUserStorage): Observable<Array<IChat>> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<Array<IChat>>(`${this.url}getChatsPreviewByField/${account.login}`,
      {headers, params: {field}});
  }

  public getChat(chatId: string, account: IUserStorage): Observable<IChat> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<IChat>(`${this.url}getChat/${account.login}/${chatId}`,
      {
        headers,
        params: {
          limit: 25
        }
      });
  }

  public getMessages(chatId: string, account: IUserStorage, offset: number, limit: number = 25): Observable<IChat> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<IChat>(`${this.url}getMessages/${account.login}/${chatId}`,
      {
        headers,
        params: {
          offset,
          limit
        }
      });
  }
}
