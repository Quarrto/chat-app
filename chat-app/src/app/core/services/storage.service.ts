import { Injectable } from '@angular/core';
import { IUserStorage } from '../interfaces/user-storage.interface';
import * as _ from 'lodash';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  public storageChanged = new Subject();

  constructor() {
    if (!localStorage.getItem('users')) {
      this.resetAllUsers();
    }
  }

  public addNewUser(login: string, token: string) {
    const users: Array<IUserStorage> = JSON.parse(localStorage.getItem('users') || '[]');

    if (users.find((value => value.login === login))) {
      throw new Error('User already exist');
    }

    users.push({login, sessionToken: token});

    localStorage.setItem('users', JSON.stringify(users));
    this.storageChanged.next();
  }

  public deleteUser(login: string) {
    const users: Array<IUserStorage> = JSON.parse(localStorage.getItem('users') || '[]');

    _.pullAllBy(users, [{login}], 'login');

    localStorage.setItem('users', JSON.stringify(users));
    this.storageChanged.next();
  }

  public getUser(login: string): IUserStorage | undefined {
    const users: Array<IUserStorage> = JSON.parse(localStorage.getItem('users') || '[]');

    return users.find((value => value.login === login));
  }

  public getAllUsers(): Array<IUserStorage> {
    return JSON.parse(localStorage.getItem('users') || '[]');
  }

  public resetAllUsers() {
    localStorage.setItem('users', JSON.stringify([]));
    this.storageChanged.next();
  }
}
