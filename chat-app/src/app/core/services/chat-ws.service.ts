import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebsocketService } from '../../websocket/websocket.service';
import { IAccount } from '../interfaces/account.interface';
import { IChat } from '../interfaces/chat-preview.interface';
import { IMessageEvent } from '../interfaces/message-event.interface';
import { IOnlineStatus } from '../interfaces/online-status.interface';
import { ITypingStatus } from '../interfaces/typing-status.interface';
import { IUserStorage } from '../interfaces/user-storage.interface';
import { IWsError } from '../interfaces/ws-error.interface';
import { EventService } from './event.service';

@Injectable({
  providedIn: 'root'
})
export class ChatWsService {

  constructor(private ws: WebsocketService, private events: EventService) {
  }

  public connect(account: IUserStorage) {
    this.ws.addOnConnectFn(this.auth.bind(this, account));

    this.ws.connect();

    // this.ws.send('connect', account);
  }

  public disconnect(): Observable<boolean> {
    return this.ws.disconnect();
  }

  public sendMessage(account: IUserStorage, chatId: string, message: string) {
    this.ws.send('message', {chatId, message, owner: account.login});
    this.events.send<IMessageEvent>('message', {chatId, send: true});
  }

  public onMessage(): Observable<IChat> {
    // this.events.send('message');
    return (this.ws.on('message') as Observable<IChat>);
    // .pipe(
    //   tap(x => this.events.send<IMessageEvent>('message', {chatId: x.chatId, send: false}))
    // );
  }

  public sendTyping(account: IUserStorage, chatId: string, typing: boolean) {
    this.ws.send('typing', {chatId, typing, owner: account.login});
  }

  public onTyping(): Observable<ITypingStatus> {
    return this.ws.on('typing') as Observable<ITypingStatus>;
  }

  public sendRead(account: IUserStorage, chatId: string) {
    this.ws.send('read', {chatId, owner: account.login});
    this.events.send<IMessageEvent>('read', {chatId, send: true});
  }

  public onRead(): Observable<IChat> {
    // this.events.send('read');
    return (this.ws.on('read') as Observable<IChat>);
    // .pipe(
    //   tap(x => this.events.send<IMessageEvent>('read', {chatId: x.chatId, send: false}))
    // );
  }

  public onOnline(): Observable<IOnlineStatus> {
    return this.ws.on('online') as Observable<IOnlineStatus>;
  }

  public onError(): Observable<IWsError> {
    return this.ws.on('error') as Observable<IWsError>;
  }

  public onFriends(): Observable<IAccount> {
    return this.ws.on('friends') as Observable<IAccount>;
  }

  private auth(account: IAccount) {
    this.ws.send('connect', account);
  }
}
