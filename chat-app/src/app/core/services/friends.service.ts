import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAccount } from '../interfaces/account.interface';
import { environment } from '../../../environments/environment';
import { IUserStorage } from '../interfaces/user-storage.interface';
import { IInboxValues } from '../interfaces/inbox-values.interface';

@Injectable({
  providedIn: 'root'
})
export class FriendsService {
  private url = environment.backendURL + 'friend/';

  constructor(private httpClient: HttpClient) {
  }

  public search(query: string, account: IUserStorage): Observable<Array<string>> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<Array<string>>(`${this.url}searchFriends/`, {headers, params: {login: query}});
  }

  public getInvites(account: IUserStorage): Observable<IAccount> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<IAccount>(`${this.url}getFriendsInvites/${account.login}`, {headers});
  }

  public add(login: string, account: IUserStorage): Observable<IAccount> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.put<IAccount>(`${this.url}addFriend/${account.login}`, {login}, {headers});
  }

  public decline(login: string, account: IUserStorage): Observable<IAccount> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.put<IAccount>(`${this.url}declineFriend/${account.login}`, {login}, {headers});
  }

  public delete(login: string, account: IUserStorage): Observable<IAccount> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.put<IAccount>(`${this.url}deleteFriend/${account.login}`, {login}, {headers});
  }

  public addImportant(login: string, account: IUserStorage): Observable<Array<string>> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.put<Array<string>>(`${this.url}addImportant/${account.login}`, {login}, {headers});
  }

  public deleteImportant(login: string, account: IUserStorage): Observable<Array<string>> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.put<Array<string>>(`${this.url}deleteImportant/${account.login}`, {login}, {headers});
  }

  public getValues(account: IUserStorage): Observable<IInboxValues> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<IInboxValues>(`${this.url}getValues/${account.login}`, {headers});
  }
}
