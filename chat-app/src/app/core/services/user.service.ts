import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IUserStorage } from '../interfaces/user-storage.interface';
import { Observable } from 'rxjs';
import { IUserInfo } from '../interfaces/user-info.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = environment.backendURL + 'user/';

  constructor(private httpClient: HttpClient) {
  }

  public getUserInfo(userLogin: string, account: IUserStorage): Observable<IUserInfo> {
    const headers = new HttpHeaders()
      .append('Accept', ['text/html', 'application/json'])
      .append('Authorization', `Bearer ${account.sessionToken}`);

    return this.httpClient.get<IUserInfo>(`${this.url}getUserInfo/${userLogin}`,
      {headers});
  }
}
