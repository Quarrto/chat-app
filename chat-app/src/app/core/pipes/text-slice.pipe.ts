import { Pipe, PipeTransform } from '@angular/core';

/**
 * Shortens the text and adding dots after if it needed
 */
@Pipe({
  name: 'textSlice'
})
export class TextSlicePipe implements PipeTransform {
  public transform(value: string = '', end: number = value?.length || 0): string {
    if (value.length > end) {
      return value.slice(0, end) + '...';
    } else {
      return value;
    }
  }
}
