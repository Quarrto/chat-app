import { Pipe, PipeTransform } from '@angular/core';
import { GenderEnum } from '../enums/gender.enum';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    switch (value as GenderEnum) {
      case GenderEnum.MALE:
        return 'Male';
      case GenderEnum.FEMALE:
        return 'Female';
      case GenderEnum.UNMARKED:
      default:
        return 'Not specified';
    }
  }

}
