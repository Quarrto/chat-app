import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'messageDate'
})
export class MessageDatePipe implements PipeTransform {

  /**
   * Return date
   * in format hh:mm : if  difference between now and dateNum less than 1 day
   * in format DD:MM : if difference between now and dateNum less than 1 year
   * in format YYYY : otherwise
   * @param dateNum
   * @param args
   */
  transform(dateNum: number | Date | undefined, ...args: unknown[]): string {
    if (!dateNum) {
      return '';
    }

    const date = new Date(dateNum as number | Date);
    const now = new Date();

    if (+now - +date < 24 * 60 * 60 * 1000) {
      let hours = date.getHours().toString();
      let minutes = date.getMinutes().toString();

      if (hours.length === 1) {
        hours = '0' + hours;
      }

      if (minutes.length === 1) {
        minutes = '0' + minutes;
      }

      return `${hours}:${minutes}`;
    }

    const isLeap = new Date(date.getFullYear(), 1, 29).getMonth() === 1;

    if (+now - +date < (isLeap ? 366 : 365) * 24 * 60 * 60 * 1000) {
      let day = date.getDate().toString();
      let month = (date.getMonth() + 1).toString();

      if (day.length === 1) {
        day = '0' + day;
      }

      if (month.length === 1) {
        month = '0' + month;
      }

      return `${day}.${month}`;
    }

    return `${date.getFullYear()}`;
  }

}
