export const environment = {
  production: true,
  backendURL: 'https://salty-plateau-40871-new.herokuapp.com/',
  wsURL: 'wss://salty-plateau-40871-new.herokuapp.com/ws/'
};
